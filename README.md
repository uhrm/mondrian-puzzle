# Mondrian Puzzle Solver

## Basic usage

Execute from project directory

```
dotnet run <N> <V>
```

where `<N>` is the problem size (i.e. solving for an `N`x`N` square) and `<V>` is the desired gap between largest and smallest piece.

The program tries to find a solution with the desired gap or verifies that no such solution exists.

### Example:

```
dotnet run 11 6
```
should output
```
[2019-12-20 19:28:39] Start search: grid 11x11, gap 6
[2019-12-20 19:28:39] Candidate (4 x 3), (6 x 2), (7 x 2), (5 x 3), (4 x 4), (8 x 2), (6 x 3), (9 x 2)
[2019-12-20 19:28:39] Solution found (grid 11x11, gap 6)
                      00002233333
                      00002233333
                      00002233333
                      44442266655
                      44442266655
                      44442266655
                      44442266655
                      11111166655
                      11111166655
                      77777777755
                      77777777755
                      Solution statistics
                       - time       0:00:00.016
                       - candidates 1
                       - nodes      1963
```

### References

- https://oeis.org/A276523
- https://mondrianpuzzle.appspot.com


## Algorithm

The program computes the set of candidates, i.e. all subsets of pieces that differ in size at most `V` and sum to the square size `N`x`N`. For each candidate, it tries to find a placement of the pieces that fills the square.

### Candidates

This uses an implementation of the text book dynamic programming algorithm that solves the subset sum problem in pseudo-polynomial time (note that due to the problem size definition of the Mondrian Puzzle, it runs in *actual* polynomial time).

It uses the tableau from the dynamic programming algorithm to enumerate all feasible solutions, which are the candidate subsets.

### Placement

This part uses a standard backtracking algorithm. It tries to place the pieces at the 'top most-then-left most' position and tracks back when that position is infeasible for all remaining pieces.

This placement strategy offers several possibilities for implementing the feasiblity checks and add/remove operations of the pieces.


## Profiling

Run debug binary

```
dotnet bin/Debug/netcoreapp3.0/mondrian-puzzle.dll 22 9
```

Attach tracing profiler

```
dotnet trace collect -p <PID> --format speedscope
```
where `<PID>` is the process ID of the first command.

Note that the trace tool is not installed by default. See first reference below for setup instructions.

Running the trace tool produces a `trace.speedscope.json` file. To view the results, open this file with [Speedscope](https://www.speedscope.app/).

### References

1. https://github.com/dotnet/diagnostics/blob/master/documentation/dotnet-trace-instructions.md
2. https://www.speedscope.app/

using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace Mondrian
{
    public sealed class Placer3
    {
        // Try to place oriented piece (w,h) at position (x,y). If
        // successful, the functions returns true and updates 'p'
        // with the location where to place the next piece. Otherwise,
        // the function returns false and does not modify the
        // variable 'p'.
        private static bool TryAdd(int n, int x, int y, int w, int h, int[] next, ref (int x,int y) p)
        {
            if (w > n-x || h > n-y) {
                return false;
            }
            // check for piece intersections
            for (var tx=x; tx<x+w; tx++) {
                if (next[tx] > y) {
                    return false;
                }
            }
            // update array of next placement locations
            for (var tx=x; tx<x+w; tx++) {
                Debug.Assert(next[tx] == y);
                next[tx] += h;
            }
            // find next placement location
            var nx = -1;
            var ny = Int32.MaxValue;
            for (var k=0; k<n; k++) {
                if (next[k] < ny) {
                    nx = k;
                    ny = next[k];
                }
            }
            p = (nx, ny);
            return true;
        }

        private static void Remove(int n, int x, int y, int w, int h, int[] next)
        {
            // update array of next placement locations
            for (var tx=x; tx<x+w; tx++) {
                Debug.Assert(next[tx] == y+h);
                next[tx] -= h;
            }
        }

        public static ((int x,int y,int w,int h,int i)[]?, long[] nodestats) Find(int n, (int w,int h)[] candidates, CancellationToken token)
        {
            var m = candidates.Length;
            var m2 = 2*m;

            // node placement statistics (histogram by depth)
            var nodestats = new long[m];

            // oriented pieces
            var pieces = new (int w,int h)[m2];
            for (var i=0; i<m2; i++) {
                var k = i >> 1;
                int w, h;
                if ((i & 0x1) == 0) {
                    (w, h) = candidates[k];
                }
                else {
                    (h, w) = candidates[k];
                }
                pieces[i] = (w, h);
            }

            // prune symmetric solutions
            // flag bits:
            //   0: prune oriented piece at all depths ((h,w) orientation of quadratic pieces)
            //   1: prune oriented piece at depth = 0
            var prune = new int[m2];
            for (var i=1; i<m2; i+=2) {
                var (w, h) = pieces[i];
                if (w == h) {
                    prune[i] |= 0x01;
                    prune[i-1] |= 0x02; // FIXME: this is only safe for three or less quadratic pieces in the candidate set
                }
                prune[i] |= 0x02;
            }

            var p = (x:0,y:0); // current position
            var next = new int[n]; // array of next y-placement position (for all x positions)
            var depth32 = new int[m2];
            var depth64 = MemoryMarshal.Cast<int,long>(depth32);
            Array.Fill(depth32, -1);
            var stack = new (int x,int y,int i)[m];
            Array.Fill(stack, (-1, -1, -1));
            var top = 0; // current depth
            while (top >= 0 && top < m && !token.IsCancellationRequested) {
                var (_, _, i) = stack[top];
                // find next piece to try
                var mask = top == 0 ? 0x3 : 0x1;
                do {
                    i++;
                    var k = i >> 1; // piece index
                    while (k < m && depth64[k] != -1L) {
                        k++;
                        i = k << 1;
                    }
                } while (i < m2 && (prune[i] & mask) != 0);
                if (i < m2) {
                    nodestats[top]++;
                    // if (nnodes == 20) {
                    //     return (null, nnodes);
                    // }
                    var (x, y) = p;
                    stack[top] = (x, y, i);
                    var (w, h) = pieces[i];
                    // Console.WriteLine($"*** top = {top}, i = {i} (k={k}), x = {x}, y = {y}, w = {w}, h = {h}");
                    if (TryAdd(n, x, y, w, h, next, ref p)) {
                        // Console.WriteLine($"*** add top {top}: piece {i} ({w} x {h}) at ({x},{y})");
                        // place piece
                        depth32[i] = top;
                        top++;
                        // Print(n, pieces, stack, top, p);
                        if (top == m) {
                            var result = stack.Select(s => (s.x, s.y, pieces[s.i].w, pieces[s.i].h, s.i>>1)).ToArray();
                            return (result, nodestats);
                        }
                        if (y == 0 && x+w == n) {
                            // prune symmetric solutions mirrored at y-axis
                            // if ((prune[i] & 0x2) == 0) {
                            //     Console.WriteLine($"*** prune piece {i} [y] (stack: [{String.Join(", ", stack[0..top].Select(s=>s.i))}])");
                            //     Print(n, pieces, stack, top, p);
                            // }
                            prune[i] |= 0x2;
                        }
                        else if (x == 0 && y+h == n) {
                            // prune symmetric solutions rotated by -90°
                            // if ((prune[i^1] & 0x2) == 0) {
                            //     Console.WriteLine($"*** prune piece {i^1} [-] (stack: [{String.Join(", ", stack[0..top].Select(s=>s.i))}])");
                            //     Print(n, pieces, stack, top, p);
                            // }
                            prune[i^1] |= 0x2;
                        }
                    }
                }
                else {
                    // backtrack
                    stack[top] = (-1, -1, -1);
                    top--;
                    if (top >= 0) {
                        (p.x, p.y, i) = stack[top];
                        depth32[i] = -1;
                        var (w, h) = pieces[i];
                        // Console.WriteLine($"*** del top {top}: piece {i} ({w} x {h}) at ({x},{y})");
                        Remove(n, p.x, p.y, w, h, next);
                    }
                }
            }
            return (null, nodestats);
        }

        private static void Print(int n, (int w,int h)[] pieces, (int x,int y,int i)[] stack, int top, (int x,int y) p)
        {
            const string symbols = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Console.WriteLine($"*** stack: [{String.Join(", ", stack[0..top].Select(s=>symbols[s.i>>1]))}]");
            Console.WriteLine($"*** p = ({p.x},{p.y})");
            for (var y=0; y<n; y++) {
                Console.Write("*** ");
                for (var x=0; x<n; x++) {
                    var t = 0;
                    for (; t<top; t++) {
                        var (tx, ty, ti) = stack[t];
                        var (tw, th) = pieces[ti];
                        if (tx <= x && x < tx+tw && ty <= y && y < ty+th) {
                            Console.Write(symbols[ti>>1]);
                            break;
                        }
                    }
                    if (t == top) {
                        Console.Write(x==p.x && y==p.y ? "*" : ".");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
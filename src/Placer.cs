using System;
using System.Diagnostics;
using System.Linq;

namespace Mondrian
{
    public sealed class Placer
    {
        // Try to place piece (w,h) at position (x,y). If successful, the
        // return value is the location where to place the next tile.
        // Otherwise, the function returns null.
        private static (int x, int y)? Add(int n, int x, int y, int w, int h, byte[,] F)
        {
            if (w > n-x || h > n-y) {
                return null;
            }
            // check for collisions (note: we ony need to check the top row
            // when the placement strategy is 'top most-then-left most')
            for (var i=0; i<w; i++) {
                if (F[x+i,y] != 0) {
                    return null;
                }
            }
            // mark occupied area
            for (var i=0; i<w; i++) {
                for (var j=0; j<h; j++) {
                    Debug.Assert(F[x+i,y+j] == 0);
                    F[x+i,y+j] = 1;
                }
            }
            // find next placement location
            while (y < n) {
                while (x < n) {
                    if (F[x,y] == 0) {
                        return (x, y);
                    }
                    x++;
                }
                y++;
                x = 0;
            }
            return (x, y);
        }

        private static void Remove(int n, int x, int y, int w, int h, byte[,] F)
        {
            Debug.Assert(x >= 0 && y >= 0);
            Debug.Assert(x+w <= n && y+h <= n);
            for (var i=0; i<w; i++) {
                for (var j=0; j<h; j++) {
                    Debug.Assert(F[x+i,y+j] == 1);
                    F[x+i,y+j] = 0;
                }
            }
        }

        public static ((int x,int y,int i)[]?,long nnodes) Find(int n, (int w,int h)[] pieces)
        {
            var nnodes = 0L;
            var m = pieces.Length;
            var k = new int[m];
            Array.Fill(k, -1);
            var F = new byte[n,n];
            var p = (x:0,y:0); // current position
            var stack = new (int x,int y,int i)[m];
            Array.Fill(stack, (-1, -1, -1));
            var top = 0; // current depth
            while (top >= 0 && top < m) {
                Debug.Assert(F[p.x, p.y] == 0);
                // find next piece to try
                var q = ((int,int)?)null;
                var (x, y, i) = stack[top];
                do {
                    if (i == -1) {
                        do {
                            i++;
                        } while (i < m && k[i] >= 0);
                        if (i == m) {
                            break;
                        }
                    }
                    else {
                        var (w, h) = pieces[i];
                        if (x >= 0 && y >= 0) {
                            // Console.WriteLine($"*** del top {top}: piece {i} ({w} x {h}) at ({x},{y})");
                            Remove(n, x, y, w, h, F);
                            p = (x, y);
                            x = y = -1;
                            stack[top] = (-1, -1, -1);
                        }
                        pieces[i] = (h, w);
                        if (w <= h) {
                            k[i] = -1;
                            do {
                                i++;
                            } while (i < m && k[i] >= 0);
                            Debug.Assert(i == m || pieces[i].w >= pieces[i].h);
                            if (i == m) {
                                break;
                            }
                        }
                    }
                    q = Add(n, p.x, p.y, pieces[i].w, pieces[i].h, F);
                    nnodes++;
                    // if (q != null) {
                    //     Console.WriteLine($"*** add top {top}: piece {i} ({pieces[i].w} x {pieces[i].h}) at ({p.x},{p.y}) [q = ({q.Value.Item1},{q.Value.Item2})]");
                    // }
                } while (q == null);
                if (i < m) {
                    // place piece
                    Debug.Assert(q != null);
                    k[i] = top;
                    stack[top++] = (p.x, p.y, i);
                    p = q.Value;
                    // Print(n, pieces, stack, top, p, F);
                    if (top == m) {
                        return (stack, nnodes);
                    }
                }
                else {
                    // backtrack
                    top--;
                }
            }
            return (null, nnodes);
        }

        private static void Print(int n, (int w,int h)[] pieces, (int x,int y,int i)[] stack, int top, (int x,int y) p, byte[,] F) {
            Console.WriteLine($"*** stack: [{String.Join(", ", stack[0..top].Select(s=>s.i))}]");
            Console.WriteLine($"*** p = ({p.x},{p.y})");
            for (var y=0; y<n; y++) {
                Console.Write("*** ");
                for (var x=0; x<n; x++) {
                    var t = 0;
                    for (; t<top; t++) {
                        var (tx, ty, ti) = stack[t];
                        if (tx <= x && x < tx+pieces[ti].w && ty <= y && y < ty+pieces[ti].h) {
                            Console.Write(ti);
                            break;
                        }
                    }
                    if (t == top) {
                        Debug.Assert(F[x,y] == 0);
                        Console.Write(".");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
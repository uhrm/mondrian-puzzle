using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using System.Threading;

namespace Mondrian
{
    // Placement strategy 8
    //
    // Phase 3: Interior pieces
    public sealed unsafe partial class Placer8
    {
        // find next placement location: top-most, left-most free corner
        private (int cx,int cy) NextP0()
        {
            var nw = this.nw;
            var nh = this.nh;
            var square = this.horz_square;
            for (var cy = 0; cy < nh; cy++, square++) {
                var cx = (int)Bmi1.X64.TrailingZeroCount(~(*square));
                if (cx < nw) {
                    return (cx, cy);
                }
            }
            // Print(top, (-1, -1));
            // Debug.Assert(false); // should never reach
            throw new InvalidOperationException("No free tile in square.");
        }

        // find next placement location: minimum x-length
        private (int cx,int cy) NextP1()
        {
            var nw = this.nw;
            var nh = this.nh;
            var square = this.horz_square;

            var cwmin = Int32.MaxValue;
            var cx = -1;
            var cy = -1;

            var a = (1UL << nw) - 1; // current row
            for (var y = 0; y < nh; y++, square++) {

                var b = a; // previous row
                a = *square;
    
                var xl = b & ~a & ((a << 1) | 1);
                // print(f"xl = {disp(n, xl)}")
    
                var xr = b & ~a & ((a >> 1) | 1UL<<(nw-1));
                // print(f"xr = {disp(n, xr)}")
    
                while (xl > 0) {
                    var il = (int)Bmi1.X64.TrailingZeroCount(xl);
                    xl &= ~(1UL << il);
                    var ir = (int)Bmi1.X64.TrailingZeroCount(xr);
                    xr &= ~(1UL << ir);

                    if (ir-il < cwmin) {
                        cwmin = ir - il;
                        cx = il;
                        cy = y;
                    }
                }
            }
            Debug.Assert(cx >= 0 && cx < nw && cy >= 0 && cy < nh);

            return (cx, cy);
        }

        // find next placement location: minimum x-length + minimum y-length
        // private (int cc,int cx,int cy,int cw,int ch) NextP3()
        // {
        //     var nw = this.nw;
        //     var nh = this.nh;
        //     var rms = this.horz_square;
        //     var cms = this.vert_square;

        //     Transpose64((byte*)rms, (byte*)cms);

        //     var cc = -1;
        //     var cwmin = nw+1;
        //     var chmin = nh+1;
        //     var cx = -1;
        //     var cy = -1;

        //     var a = (1UL << nw) - 1; // current row
        //     for (var y = 0; y < nh; y++, rms++) {

        //         var b = a; // previous row
        //         a = *rms;
    
        //         var xl = /*b &*/ ~a & ((a << 1) | 1) & ((1UL << nw) - 1);
        //         // print(f"xl = {disp(n, xl)}")
    
        //         var xr = /*b &*/ ~a & ((a >> 1) | 1UL<<(nw-1)) & ((1UL << nw) - 1);
        //         // print(f"xr = {disp(n, xr)}")
    
        //         while (xl > 0) {
        //             var il = (int)Bmi1.X64.TrailingZeroCount(xl);
        //             xl &= ~(1UL << il);
        //             var ir = (int)Bmi1.X64.TrailingZeroCount(xr);
        //             xr &= ~(1UL << ir);
        //             Debug.Assert(0 <= il && il <= ir && ir < nw, $"y={y}: 0 <= {il} <= {ir} < {nw}, a = {a,00000000:X}, b = {b,00000000:X}");

        //             if ((b & (1UL << il)) != 0) {
        //                 var c = cms[il];
        //                 var yt = ~c & ((c << 1) | 1UL) & ~((1UL<<y)-1);
        //                 var it = (int)Bmi1.X64.TrailingZeroCount(yt);
        //                 var yb = ~c & ((c >> 1) | 1UL<<(nh-1)) & ~((1UL<<y)-1);
        //                 var ib = (int)Bmi1.X64.TrailingZeroCount(yb);
        //                 // Debug.Assert(it <= ib, $"{it} <= {ib}");

        //                 if (ir-il + ib-it < cwmin+chmin) {
        //                     cc = 0;
        //                     cwmin = ir - il;
        //                     chmin = ib - it;
        //                     cx = il;
        //                     cy = y;
        //                 }
        //             }

        //             if ((b & (1UL << ir)) != 0) {
        //                 var c = cms[ir];
        //                 var yt = ~c & ((c << 1) | 1UL) & ~((1UL<<y)-1);
        //                 var it = (int)Bmi1.X64.TrailingZeroCount(yt);
        //                 var yb = ~c & ((c >> 1) | 1UL<<(nh-1)) & ~((1UL<<y)-1);
        //                 var ib = (int)Bmi1.X64.TrailingZeroCount(yb);
        //                 // Debug.Assert(it <= ib, $"{it} <= {ib}");

        //                 if (ir-il + ib-it < cwmin+chmin) {
        //                     cc = 1;
        //                     cwmin = ir - il;
        //                     chmin = ib - it;
        //                     cx = ir;
        //                     cy = y;
        //                 }
        //             }
        //         }
        //     }
        //     Debug.Assert(cx >= 0 && cx < nw && cy >= 0 && cy < nh, $"0 <= {cx} < {nw} and 0 <= {cy} < {nh}");

        //     return (cc, cx, cy, cwmin+1, chmin+1);
        // }

        private void Transpose64(byte *src, byte* dst)
        {
            // transpose 64x64 bit matrix
            //
            //  * divide into 8x2 blocks (of size 8bits x 32bits which fit into a 256 bit vector register)
            //  * transpose each block using AVX2 vector intrinsics (innermost k-loop)
            //  * hide latency of 'gather' instruction by preloading one block ahead
            //
            //     - VINC = [   4,   4,   4,   4,   4,   4,   4,   4 ]
            //     - for i = 0..8
            //         - VOFS = [   0,  8,  16,  24, 32, 40, 48, 56 ]
            //         - REG = _mm256_i32gather_epi32(&SRC[8*i,0], [ 0,  8, 16, 24, 32, 40, 48, 56 ], 1)    # [preload] gather 8 32bit words with 8 byte spacing into 256bit register
            //         - PRE = _mm256_i32gather_epi32(&SRC[8*i,0], [ 4, 12, 20, 28, 36, 44, 52, 60 ], 1)    # [preload] gather 8 32bit words with 8 byte spacing into 256bit register
            //         - for k = 0..31
            //             - DST[31-k,i] = _mm256_movemask_epi8(REG)    # movemask 8 MSBs from packed 32bit words into line 31-i
            //             - REG = _mm256_slli_epi32(REG, 1)            # shift left 1 bit 8 packed 32bit words
            //         - REG = PRE
            //         - for k = 0..31
            //             - DST[63-k,i] = _mm256_movemask_epi8(REG)    # movemask 8 MSBs from packed 32bit words into line 31-i
            //             - REG = _mm256_slli_epi32(REG, 1)            # shift left 1 bit 8 packed 32bit words
            var vofs0 = Vector256.Create(0,  8, 16, 24, 32, 40, 48, 56);
            var vofs1 = Vector256.Create(4, 12, 20, 28, 36, 44, 52, 60);
            // Console.WriteLine("****");
            for (var i=0; i<8; i++) {
                var reg = Avx2.GatherVector256(((int*)src)+16*i, vofs0, 1);
                var pre = Avx2.GatherVector256(((int*)src)+16*i, vofs1, 1);
                for (var k=0; k<32; k++) {
                    dst[8*(31-k)+i] = (byte)Avx2.MoveMask(reg.AsSingle());
                    reg = Avx2.ShiftLeftLogical(reg, 1);
                    // Console.WriteLine($" {Convert.ToString(dst[8*(31-k)+i], 2)}");
                }
                // Console.WriteLine();
                // return;
                reg = pre;
                for (var k=0; k<32; k++) {
                    dst[8*(63-k)+i] = (byte)Avx2.MoveMask(reg.AsSingle());
                    reg = Avx2.ShiftLeftLogical(reg, 1);
                }
            }
        }

        private void Transpose256(byte *src, byte* dst)
        {
            // transpose 256x256 bit matrix
            //
            //  * divide into 32x8 blocks (of size 8bits x 32bits which fit into a 256 bit vector register)
            //  * transpose each block using AVX2 vector intrinsics (innermost k-loop)
            //  * hide latency of 'gather' instruction by preloading one block ahead
            //
            //     - VINC = [   4,   4,   4,   4,   4,   4,   4,   4 ]
            //     - for i = 0..31
            //         - VOFS = [   0,  32,  64,  96, 128, 160, 192, 224 ]
            //         - PRE = _mm256_i32gather_epi32(&SRC[8*i,0], VOFS, 1)           # [preload] gather 8 32bit words with 32 byte spacing into 256bit register
            //         - for j = 0..7
            //             - REG = PRE
            //             - if j < 7
            //                 - VOFS = _mm256_add_epi32(VOFS, VINC)
            //                 - PRE = _mm256_i32gather_epi32(SRC[8*i,0], VOFS, 1)    # [preload] gather 8 32bit words with 32 byte spacing into 256bit register
            //             - for k = 0..31
            //                 - DST[32*j+31-k,i] = _mm256_movemask_epi8(REG)         # movemask 8 MSBs from packed 32bit words into line 31-k
            //                 - REG = _mm256_slli_epi32(REG, 1)                      # shift left 1 bit 8 packed 32bit words
            var vinc = Vector256.Create(4, 4, 4, 4, 4, 4, 4, 4);
            for (var i=0; i<32; i++) {
                var vofs = Vector256.Create(0, 32, 64, 96, 128, 160, 192, 224);
                var pre = Avx2.GatherVector256(((int*)src)+64*i, vofs, 1);
                for (var j=0; j<8; j++) {
                    var reg = pre;
                    if (j < 7) {
                        vofs = Avx2.Add(vofs, vinc);
                        pre = Avx2.GatherVector256(((int*)src)+64*i, vofs, 1);
                    }
                    var p = dst + 256*j + 32*31 + i;
                    for (int k=0; k<32; k++, p-=32) {
                        *p = (byte)Avx2.MoveMask(reg.AsSingle());
                        Avx2.ShiftLeftLogical(reg, 1);
                    }
                }
            }
        }

        public bool FindInterior()
        {
            var pieces = this.pieces;
            (int i,int cx,int cy)[] stack = this.stack;
            var square = this.horz_square;
            var nodestats = this.nodestats;
            var token = this.token;

            var nw = this.nw;
            var nh = this.nh;

            var nedges = this.nedges;

            var m = pieces.Length;

            if (nedges == (m>>1)) {
                return true; // no interior pieces to fill
            }

            int cx = 0, cy = 0; // current placement position
            var top = nedges; // current depth
            int i = -1, k; // oriented piece index, piece index
            int w, h; // piece width, height
            ulong mask;
            while (true) {
                // find next placement position
                // (cx, cy) = NextP0();
                (cx, cy) = NextP1();

            next:
                // find next piece to try
                i++;
                k = i >> 1;
                k = (int)Bmi1.X64.TrailingZeroCount(~(this.taken | ((1UL<<k)-1UL)));
                i = Math.Max(i, k<<1);
                if (i == m) {
                    goto backtrack;
                }
                Debug.Assert(i < m && k < (m>>1));

                nodestats[top]++;
                (w, h) = pieces[i];
                // Console.WriteLine($"*** interior: top = {top,2}, i = {i,2}, (cx,cy) = ({cx,2},{cy,2}), (w,h) = ({w,2},{h,2})");


                if (w > nw-cx || h > nh-cy) {
                    goto next; // continue w/o recomputing cx/cy
                }
                // check for collisions
                mask = ((1UL << w) - 1UL) << cx;
                for (var sy=cy+h-1; sy>=cy; sy--) {
                    if ((square[sy] & mask) != 0UL) {
                        goto next; // continue w/o recomputing cx/cy
                    }
                }
                // mark occupied area
                for (var sy=cy+h-1; sy>=cy; sy--) {
                    square[sy] |= mask;
                }


                stack[top++] = (i, cx, cy);
                this.taken |= 1UL << k;
                // Console.WriteLine($"*** interior: add top = {top-1,2}, i = {i,2}, (cx,cy) = ({cx,2},{cy,2}), (w,h) = ({w,2},{h,2})");
                // Print(top, (cx, cy));
                if (top == (m>>1)) {
                    return true; // found a solution
                }
                i = -1;
                continue;
            backtrack:
                if (top == nedges) {
                    break; // no solution found
                }
                (i, cx, cy) = stack[--top];
                k = i >> 1;
                this.taken &= ~(1UL << k);
                (w, h) = pieces[i];


                Debug.Assert(cx >= 0 && cy >= 0 && cx+w <= nw && cy+h <= nh);
                mask = ~(((1UL << w) - 1UL) << cx);
                for (var sy=cy; sy<cy+h; sy++) {
                    Debug.Assert((square[sy] & ~mask) == ~mask);
                    square[sy] &= mask;
                }


                // Console.WriteLine($"*** interior: del top = {t,2}, i = {i,2}, (cx,cy) = ({cx,2},{cy,2}), (kw,kh) = ({w,2},{h,2})");
            }
            return false;
        }

        private IEnumerable<(int x,int y,int w,int h,int i)> ReconstructInterior()
        {
            var pieces = this.pieces;
            (int i,int cx,int cy)[] stack = this.stack;

            var m = stack.Length;

            var top = this.nedges;
            while (top < m) {
                var (i, cx, cy) = stack[top++];
                var (w, h) = pieces[i];
                yield return (cx, cy, w, h, i>>1);
            }
        }
    }
}
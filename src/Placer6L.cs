
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Mondrian
{
    public class Placer6L
    {

        // Try to add edge piece.
        private static bool TryAddEx(int n, ulong[] square, int i, int d, int c, (int w,int h)[] pieces)
        {
            // Debug.Assert(stack[t].n == n-c, $"stack[{t}].n = {stack[t].n}, n = {n}, c = {c}");
            var k = i >> 1;
            int tw, th;
            if (((i & 0x1) ^ (d & 0x1)) == 0) {
                (tw, th) =  pieces[k];
            }
            else {
                (th, tw) = pieces[k];
            }
            // Console.WriteLine($"*** layout: t={t}, i={i}, d={d}, tw={tw}, th={th}");
            switch (d) {
            case 0: {
                var mask = ((1UL << tw) - 1UL) << c;
                for (var ty=0; ty<th; ty++) {
                    square[ty] |= mask;
                }
                break;
            }
            case 1: {
                var mask = ((1UL << tw) - 1UL) << n-tw;
                // check for intersections
                for (var ty=c; ty<c+th; ty++) {
                    if ((square[ty] & mask) != 0UL) {
                        // Console.WriteLine($"*** layout: intersection at ({tx},{ty}), backjump to top = {t}");
                        // Print(n, square, tx, ty);
                        return false;
                    }
                }
                // mark occupied area
                for (var ty=c; ty<c+th; ty++) {
                    square[ty] |= mask;
                }
                break;
            }
            case 2: {
                var mask = ((1UL << tw) - 1UL) << c-tw+1;
                // check for intersections
                for (var ty=n-th; ty<n; ty++) {
                    if ((square[ty] & mask) != 0UL) {
                        // Console.WriteLine($"*** layout: intersection at ({tx},{ty}), backjump to top = {t}");
                        // Print(n, square, tx, ty);
                        return false;
                    }
                }
                // mark occupied area
                for (var ty=n-th; ty<n; ty++) {
                    square[ty] |= mask;
                }
                break;
            }
            case 3: {
                var mask = (1UL << tw) - 1UL;
                // check for intersections
                for (var ty=c-th+1; ty<=c; ty++) {
                    if ((square[ty] & mask) != 0UL) {
                        // Console.WriteLine($"*** layout: intersection at ({tx},{ty}), backjump to top = {t}");
                        // Print(n, square, tx, ty);
                        return false;
                    }
                }
                // mark occupied area
                for (var ty=c-th+1; ty<=c; ty++) {
                    square[ty] |= mask;
                }
                // c -= th;
                break;
            }}
            // Print(n, square, -1, -1);
            return true;
        }

        // Remove edge piece.
        private static bool RemoveEx(int n, ulong[] square, int i, int d, int c, (int w,int h)[] pieces)
        {
            // Debug.Assert(stack[t].n == n-c, $"stack[{t}].n = {stack[t].n}, n = {n}, c = {c}");
            var k = i >> 1;
            int tw, th;
            if (((i & 0x1) ^ (d & 0x1)) == 0) {
                (tw, th) =  pieces[k];
            }
            else {
                (th, tw) = pieces[k];
            }
            // Console.WriteLine($"*** layout: t={t}, i={i}, d={d}, tw={tw}, th={th}");
            switch (d) {
            case 0: {
                var mask = ~(((1UL << tw) - 1UL) << c);
                for (var ty=0; ty<th; ty++) {
                    square[ty] &= mask;
                }
                break;
            }
            case 1: {
                var mask = ~(((1UL << tw) - 1UL) << n-tw);
                for (var ty=c; ty<c+th; ty++) {
                    square[ty] &= mask;
                }
                break;
            }
            case 2: {
                var mask = ~(((1UL << tw) - 1UL) << c-tw+1);
                for (var ty=n-1; ty>=n-th; ty--) {
                    square[ty] &= mask;
                }
                break;
            }
            case 3: {
                var mask = ~((1UL << tw) - 1UL);
                for (var ty=c; ty>c-th; ty--) {
                    square[ty] &= mask;
                }
                break;
            }}
            // Print(n, square, -1, -1);
            return true;
        }

        // Try to place interior piece (w,h) at position (x,y).
        private static bool TryAddIn(int n, ulong[] square, int x, int y, int w, int h)
        {
            if (w > n-x || h > n-y) {
                return false;
            }
            // check for collisions
            var mask = ((1UL << w) - 1UL) << x;
            for (var j=y; j<y+h; j++) {
                if ((square[j] & mask) != 0UL) {
                    return false;
                }
            }
            // mark occupied area
            for (var j=y; j<y+h; j++) {
                square[j] |= mask;
            }
            return true;
        }

        // Remove interior piece (w,h) from position (x,y).
        private static void RemoveIn(int n, ulong[] square, int x, int y, int w, int h)
        {
            Debug.Assert(x >= 0 && y >= 0 && x+w <= n && y+h <= n);
            var mask = ~(((1UL << w) - 1UL) << x);
            for (var j=y; j<y+h; j++) {
                Debug.Assert((square[j] & ~mask) == ~mask);
                square[j] &= mask;
            }
        }

        // Fill interior with remaining pieces
        private static bool Fill(int n, ulong[] square, bool[] taken, (int i,int d,int n)[] stack, int top, (int w,int h)[] pieces, long[] nodestats) {
            var m = pieces.Length;
            if (top == m) {
                return true; // no interior pieces to fill
            }
            int cx = 0, cy = 0; // current position
            var t = top; // current depth
            int i = -1, k; // oriented piece index, piece index
            while (true) {
                // find next position (top-most, left-most free corner)
                for (cy = 0; cy < n; cy++) {
                    var mask = 1UL;
                    for (cx = 0; cx < n; cx++, mask <<= 1) {
                        if ((square[cy] & mask) == 0UL) {
                            goto next;
                        }
                    }
                }
                Debug.Assert(false); // should never reach
            next:
                // find next piece to try
                do {
                    i++;
                    k = i >> 1;
                } while (k < m && taken[k]);
                if (k == m) {
                    goto backtrack;
                }
                int w, h;
                if ((i & 0x1) == 0) {
                    (w, h) = pieces[k];
                }
                else {
                    (h, w) = pieces[k];
                }
                nodestats[t]++;
                // Console.WriteLine($"*** fill: top = {t,2}, i = {i,2}, (cx,cy) = ({cx,2},{cy,2}), (kw,kh) = ({w,2},{h,2})");
                if (TryAddIn(n, square, cx, cy, w, h)) {
                    // Console.WriteLine($"*** fill: add top = {t,2}, i = {i,2}, (cx,cy) = ({cx,2},{cy,2}), (kw,kh) = ({w,2},{h,2})");
                    stack[t++] = (i, cx, cy); // reuse second and third int to store placement position
                    taken[k] = true;
                    if (t == m) {
                        return true; // found a solution
                    }
                    i = -1;
                }
                continue;
            backtrack:
                if (t == top) {
                    break; // no solution found
                }
                (i, cx, cy) = stack[--t];
                k = i >> 1;
                if ((i & 0x1) == 0) {
                    (w, h) = pieces[k];
                }
                else {
                    (h, w) = pieces[k];
                }
                RemoveIn(n, square, cx, cy, w, h);
                taken[k] = false;
                // Console.WriteLine($"*** fill: del top = {t,2}, i = {i,2}, (cx,cy) = ({cx,2},{cy,2}), (kw,kh) = ({w,2},{h,2})");
            }
            return false;
        }

        private static IEnumerable<(int x,int y,int w,int h,int i)> RestoreSolution(int n, int p, (int t,int d,int n)[] stack, int top, (int w,int h)[] pieces)
        {
            // edge pieces
            var c = 0;
            for (var t=0; t<top; t++) {
                var (i, d, _) = stack[t];
                var k = i >> 1;
                int tw, th;
                if (((i & 0x1) ^ (d & 0x1)) == 0) {
                    (tw, th) =  pieces[k];
                }
                else {
                    (th, tw) = pieces[k];
                }
                switch (d) {
                case 0:
                    // Console.WriteLine($"*** solution: x={c,2} y={0,2}, w={tw,2} h={th,2} i={k,2}");
                    yield return (c, 0, tw, th, k);
                    c += tw;
                    if (c == n) {
                        c = th;
                    }
                    if (c == n) {
                        c = n-tw;
                    }
                    break;
                case 1:
                    // Console.WriteLine($"*** solution: x={n-tw,2} y={c,2}, w={tw,2} h={th,2} i={k,2}");
                    yield return (n-tw, c, tw, th, k);
                    c += th;
                    if (c == n) {
                        c = n-tw;
                    }
                    if (c == 0) {
                        c = n-th;
                    }
                    break;
                case 2:
                    // Console.WriteLine($"*** solution: x={c-tw,2} y={n-th,2}, w={tw,2} h={th,2} i={k,2}");
                    yield return (c-tw, n-th, tw, th, k);
                    c -= tw;
                    if (c == 0) {
                        c = n-th;
                    }
                    break;
                case 3:
                    // Console.WriteLine($"*** solution: x={0,2} y={c-th,2}, w={tw,2} h={th,2} i={k,2}");
                    yield return (0, c-th, tw, th, k);
                    c -= th;
                    break;
                }
            }
            // interior pieces
            var m = pieces.Length;
            for (var t=top; t<m; t++) {
                var (i, x, y) = stack[t];
                var k = i >> 1;
                int tw, th;
                if ((i & 0x1) == 0) {
                    (tw, th) =  pieces[k];
                }
                else {
                    (th, tw) = pieces[k];
                }
                // Console.WriteLine($"*** solution: x={x,2} y={y,2}, w={tw,2} h={th,2} i={k,2}");
                yield return (x, y, tw, th, k);
            }
        }

        private static void Print(int n, ulong[] square, int tx, int ty)
        {
            for (var y=0; y<n; y++) {
                Console.Write("*** ");
                var mask = 1UL;
                for (var x=0; x<n; x++, mask <<= 1) {
                    if (x==tx && y==ty) {
                        Console.Write("x");
                    }
                    else if ((square[y] & mask) != 0) {
                        Console.Write("*");
                    }
                    else {
                        Console.Write(".");
                    }
                }
                Console.WriteLine();
            }
        }

        public static ((int x,int y,int w,int h,int i)[]?, long[] nodestats) Find(int n, (int w,int h)[] pieces, CancellationToken token)
        {
            if (n >= 64) {
                throw new ArgumentException($"Unsupported problem size: expected n < 64, found n = {n}.", "n");
            }

            var m = pieces.Length;

            // search statistics
            var nodestats = new long[m];

            var square = new ulong[n];
            Array.Fill(square, 0UL);
            var taken = new bool[m];
            Array.Fill(taken, false);
            var stack = new (int i,int d,int n)[m];
            Array.Fill(stack, (-1, -1, -1));
            var dir = 0; // current search direction (0: top edge, 1: right edge, 2: bottom edge, 3: left edge)
            var nfree = n; // number of uncovered tiles (along edge 'dir')
            var top = 0; // current search depth
            int i = -1, k, p = -1; // oriented piece index, piece index, pivot piece (on stack[0])
            while (!token.IsCancellationRequested) {
                // find next piece
                do {
                    i++;
                    k = i >> 1;
                } while (k < m && taken[k]);
                // Console.WriteLine($"*** top = {top}, dir = {dir}, i = {i} (k = {k}), nfree = {nfree}");
                if (k == m) {
                    goto backtrack;
                }
                Debug.Assert(k < m);
                nodestats[top]++;
                if (top == 0) {
                    if ((i & 0x1) != 0) {
                        continue; // eliminate symmetric solutions
                    }
                    else {
                        p = k;
                    }
                }
                Debug.Assert(top == 0 || (stack[0].i >> 1) == p);
                // width and height of piece k (oriented i)
                // note: kw is the dimension along the edge 'dir', kh is perpendicular to the
                //       edge 'dir', i.e. kw is width for dir=0,2 and height for dir=1,3
                int kw, kh;
                if ((i & 0x1) == 0) {
                    (kw, kh) = pieces[k];
                }
                else {
                    (kh, kw) = pieces[k];
                }
                if (nfree < kw) {
                    continue; // piece i does not fit
                }
                if (dir < 3 && nfree == kw) {
                    if (top > 0 && k < p) {
                        continue; // eliminate symmetric solutions
                    }
                }
                // placement position of piece along edge 'dir'
                var c = dir switch {
                    0 => n-nfree, // top-left corner
                    1 => n-nfree, // top-right corner
                    2 => nfree-1, // bottom-right corner
                    3 => nfree-1+pieces[p].h, // bottom-left corner
                    _ => throw new InvalidOperationException()
                };
                if (TryAddEx(n, square, i, dir, c, pieces)) {
                    stack[top++] = (i, dir, nfree);
                    taken[k] = true;
                    i = -1;
                    nfree -= kw;
                    // Console.WriteLine($"*** add top = {top,2}, dir = {dir}, i = {i,2} (k = {k,2}), (kw,kh) = ({kw,2},{kh,2}), nfree = {nfree,2}");
                    // Print(n, square, -1, -1);
                    if (nfree == 0) {
                        dir++;
                        if (dir > 3) {
                            goto fill;
                        }
                        nfree = n - kh;
                        if (dir == 3) {
                            nfree -= pieces[p].h;
                        }
                    }
                    if (nfree == 0) {
                        dir++;
                        if (dir > 3) {
                            goto fill;
                        }
                        nfree = n - kw;
                        if (dir == 3) {
                            nfree -= pieces[p].h;
                        }
                    }
                }
                continue;
            fill:
                // Console.WriteLine($"*** fill: taken = {String.Join(", ", taken.Select(t => t ? 1 : 0))}");
                if (!Fill(n, square, taken, stack, top, pieces, nodestats)) {
                    goto backtrack;
                }
                // solution found for piece p
                var result = RestoreSolution(n, p, stack, top, pieces).ToArray();
                return (result, nodestats);
            backtrack:
                if (top == 0) {
                    break; // no solution found for piece p, continue search with next piece
                }
                (i, dir, k) = stack[--top];
                nfree = k;
                k = i >> 1;
                taken[k] = false;
                c = dir switch {
                    0 => n-nfree, // top-left corner
                    1 => n-nfree, // top-right corner
                    2 => nfree-1, // bottom-right corner
                    3 => nfree-1+pieces[p].h, // bottom-left corner
                    _ => throw new InvalidOperationException()
                };
                RemoveEx(n, square, i, dir, c, pieces);
                // Console.WriteLine($"*** del top = {top,2}, dir = {dir}, i = {i,2} (k = {k,2}), nfree = {nfree}");
            };
            return (null, nodestats);
        }

    }
}
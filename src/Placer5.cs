
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Mondrian
{
    public class Placer5
    {

        // Place all edge pieces onto square and check for intersections.
        //
        // Returns true if all pieces fit without intersecting, otherwise
        // returns false and conflict contains the index of the first
        // conflicting piece.
        private static bool Layout(int n, bool[,] square, int p, Span<(int i,int d,int n)> stack, (int w,int h)[] pieces, ref int conflict, ref long nnodes)
        {
            // Console.WriteLine($"*** layout: ({pieces[p].w},{pieces[p].h}); {String.Join(", ", stack.ToArray().Select(s=>$"({(((s.i&0x1)^(s.d&0x1))==0?pieces[s.i>>1].w:pieces[s.i>>1].h)},{(((s.i&0x1)^(s.d&0x1))==0?pieces[s.i>>1].h:pieces[s.i>>1].w)})"))}");
            // clear square
            for (var x=0; x<n; x++) {
                for (var y=0; y<n; y++) {
                    square[x,y] = false;
                }
            }
            // pivot piece
            for (var px=0; px<pieces[p].w; px++) {
                for (var py=0; py<pieces[p].h; py++) {
                    square[px,py] = true;
                }
            }
            // pieces on stack
            var m = stack.Length;
            var c = pieces[p].w;
            if (c == n) {
                c = pieces[p].h;
                Debug.Assert(stack[0].d > 0);
            }
            for (var t=0; t<m; t++) {
                var (i, d, _) = stack[t];
                // Debug.Assert(stack[t].n == n-c, $"stack[{t}].n = {stack[t].n}, n = {n}, c = {c}");
                var k = i >> 1;
                int tw, th;
                if (((i & 0x1) ^ (d & 0x1)) == 0) {
                    (tw, th) =  pieces[k];
                }
                else {
                    (th, tw) = pieces[k];
                }
                // Console.WriteLine($"*** layout: t={t}, i={i}, d={d}, tw={tw}, th={th}");
                nnodes++;
                switch (d) {
                case 0:
                    for (var tx=c; tx<c+tw; tx++) {
                        for (var ty=0; ty<th; ty++) {
                            square[tx,ty] = true;
                        }
                    }
                    c += tw;
                    if (c == n) {
                        c = th;
                        Debug.Assert(t == m-1 || stack[t+1].d > 0);
                    }
                    if (c == n) {
                        c = n-tw-1;
                        Debug.Assert(t == m-1 || stack[t+1].d > 1);
                    }
                    break;
                case 1:
                    for (var tx=n-1; tx>=n-tw; tx--) {
                        for (var ty=c; ty<c+th; ty++) {
                            if (square[tx,ty]) {
                                // Console.WriteLine($"*** layout: intersection at ({tx},{ty}), backjump to top = {t}");
                                // Print(n, square, tx, ty);
                                conflict = t;
                                return false;
                            }
                            square[tx,ty] = true;
                        }
                    }
                    c += th;
                    if (c == n) {
                        c = n-tw-1;
                        Debug.Assert(t == m-1 || stack[t+1].d > 1);
                    }
                    if (c == -1) {
                        c = n-th-1;
                        Debug.Assert(t == m-1 || stack[t+1].d > 2);
                    }
                    break;
                case 2:
                    for (var tx=c; tx>c-tw; tx--) {
                        for (var ty=n-1; ty>=n-th; ty--) {
                            if (square[tx,ty]) {
                                // Console.WriteLine($"*** layout: intersection at ({tx},{ty}), backjump to top = {t}");
                                // Print(n, square, tx, ty);
                                conflict = t;
                                return false;
                            }
                            square[tx,ty] = true;
                        }
                    }
                    c -= tw;
                    if (c == -1) {
                        c = n-th-1;
                        Debug.Assert(t == m-1 || stack[t+1].d > 2);
                        Debug.Assert(c >= 0);
                    }
                    break;
                case 3:
                    for (var tx=0; tx<tw; tx++) {
                        for (var ty=c; ty>c-th; ty--) {
                            if (square[tx,ty]) {
                                // Console.WriteLine($"*** layout: intersection at ({tx},{ty}), backjump to top = {t}");
                                // Print(n, square, tx, ty);
                                conflict = t;
                                return false;
                            }
                            square[tx,ty] = true;
                        }
                    }
                    c -= th;
                    break;
                }
            }
            // Print(n, square, -1, -1);
            return true;
        }

        // Try to place piece (w,h) at position (x,y).
        private static bool Add(int n, bool[,] square, int x, int y, int w, int h)
        {
            if (w > n-x || h > n-y) {
                return false;
            }
            // check for collisions along top border
            for (var i=0; i<w-1; i++) {
                if (square[x+i,y]) {
                    return false;
                }
            }
            // check for collisions along right border
            for (var j=0; j<h-1; j++) {
                if (square[x+w-1,y+j]) {
                    return false;
                }
            }
            // check for collisions along bottom border
            for (var i=1; i<w; i++) {
                if (square[x+i,y+h-1]) {
                    return false;
                }
            }
            // check for collisions along left border
            for (var j=1; j<h; j++) {
                if (square[x,y+j]) {
                    return false;
                }
            }
            // mark occupied area
            for (var i=0; i<w; i++) {
                for (var j=0; j<h; j++) {
                    Debug.Assert(!square[x+i,y+j]);
                    square[x+i,y+j] = true;
                }
            }
            return true;
        }

        // Remove piece (w,h) from position (x,y).
        private static void Remove(int n, bool[,] square, int x, int y, int w, int h)
        {
            Debug.Assert(x >= 0 && y >= 0);
            Debug.Assert(x+w <= n && y+h <= n);
            for (var i=0; i<w; i++) {
                for (var j=0; j<h; j++) {
                    Debug.Assert(square[x+i,y+j]);
                    square[x+i,y+j] = false;
                }
            }
        }

        // Fill interior with remaining pieces
        private static bool Fill(int n, bool[,] square, bool[] taken, (int i,int d,int n)[] stack, int top, (int w,int h)[] pieces, ref long nnodes) {
            var m = pieces.Length;
            if (top == m-1) { // note: pivot piece is not on stack
                return true; // no interior pieces to fill
            }
            int cx = 0, cy = 0; // current position
            var t = top; // current depth
            int i = -1, k; // oriented piece index, piece index
            while (true) {
                // find next position (top-most, left-most free corner)
                for (cy = 0; cy < n; cy++) {
                    for (cx = 0; cx < n; cx++) {
                        if (!square[cx,cy]) {
                            goto next;
                        }
                    }
                }
                Debug.Assert(false); // should never reach
            next:
                // find next piece to try
                do {
                    i++;
                    k = i >> 1;
                } while (k < m && taken[k]);
                if (k == m) {
                    goto backtrack;
                }
                int w, h;
                if ((i & 0x1) == 0) {
                    (w, h) = pieces[k];
                }
                else {
                    (h, w) = pieces[k];
                }
                nnodes++;
                // Console.WriteLine($"*** fill: top = {t,2}, i = {i,2}, (cx,cy) = ({cx,2},{cy,2}), (kw,kh) = ({w,2},{h,2})");
                if (Add(n, square, cx, cy, w, h)) {
                    // Console.WriteLine($"*** fill: add top = {t,2}, i = {i,2}, (cx,cy) = ({cx,2},{cy,2}), (kw,kh) = ({w,2},{h,2})");
                    stack[t++] = (i, cx, cy); // reuse second and third int to store placement position
                    taken[k] = true;
                    if (t == m-1) { // note: pivot piece is not on stack
                        return true; // found a solution
                    }
                    i = -1;
                }
                continue;
            backtrack:
                if (t == top) {
                    break; // no solution found
                }
                (i, cx, cy) = stack[--t];
                k = i >> 1;
                if ((i & 0x1) == 0) {
                    (w, h) = pieces[k];
                }
                else {
                    (h, w) = pieces[k];
                }
                Remove(n, square, cx, cy, w, h);
                taken[k] = false;
                // Console.WriteLine($"*** fill: del top = {t,2}, i = {i,2}, (cx,cy) = ({cx,2},{cy,2}), (kw,kh) = ({w,2},{h,2})");
            }
            return false;
        }

        private static IEnumerable<(int x,int y,int w,int h,int i)> RestoreSolution(int n, int p, (int t,int d,int n)[] stack, int top, (int w,int h)[] pieces)
        {
            // pivot piece
            // Console.WriteLine($"*** solution: x={0,2} y={0,2}, w={pieces[p].w,2} h={pieces[p].h,2} i={p,2}");
            yield return (0, 0, pieces[p].w, pieces[p].h, p);
            // edge pieces
            var c = pieces[p].w;
            for (var t=0; t<top; t++) {
                var (i, d, _) = stack[t];
                var k = i >> 1;
                int tw, th;
                if (((i & 0x1) ^ (d & 0x1)) == 0) {
                    (tw, th) =  pieces[k];
                }
                else {
                    (th, tw) = pieces[k];
                }
                switch (d) {
                case 0:
                    // Console.WriteLine($"*** solution: x={c,2} y={0,2}, w={tw,2} h={th,2} i={k,2}");
                    yield return (c, 0, tw, th, k);
                    c += tw;
                    if (c == n) {
                        c = th;
                    }
                    if (c == n) {
                        c = n-tw;
                    }
                    break;
                case 1:
                    // Console.WriteLine($"*** solution: x={n-tw,2} y={c,2}, w={tw,2} h={th,2} i={k,2}");
                    yield return (n-tw, c, tw, th, k);
                    c += th;
                    if (c == n) {
                        c = n-tw;
                    }
                    if (c == 0) {
                        c = n-th;
                    }
                    break;
                case 2:
                    // Console.WriteLine($"*** solution: x={c-tw,2} y={n-th,2}, w={tw,2} h={th,2} i={k,2}");
                    yield return (c-tw, n-th, tw, th, k);
                    c -= tw;
                    if (c == 0) {
                        c = n-th;
                    }
                    break;
                case 3:
                    // Console.WriteLine($"*** solution: x={0,2} y={c-th,2}, w={tw,2} h={th,2} i={k,2}");
                    yield return (0, c-th, tw, th, k);
                    c -= th;
                    break;
                }
            }
            // interior pieces
            var m = pieces.Length;
            for (var t=top; t<m-1; t++) { // note: pivot piece is not on stack
                var (i, x, y) = stack[t];
                var k = i >> 1;
                int tw, th;
                if ((i & 0x1) == 0) {
                    (tw, th) =  pieces[k];
                }
                else {
                    (th, tw) = pieces[k];
                }
                // Console.WriteLine($"*** solution: x={x,2} y={y,2}, w={tw,2} h={th,2} i={k,2}");
                yield return (x, y, tw, th, k);
            }
        }

        private static void Print(int n, bool[,] square, int tx, int ty)
        {
            for (var y=0; y<n; y++) {
                Console.Write("*** layout: ");
                for (var x=0; x<n; x++) {
                    if (x==tx && y==ty) {
                        Console.Write("x");
                    }
                    else if (square[x,y]) {
                        Console.Write("*");
                    }
                    else {
                        Console.Write(".");
                    }
                }
                Console.WriteLine();
            }
        }

        public static ((int x,int y,int w,int h,int i)[]?, long[] nodestats) Find(int n, (int w,int h)[] pieces, CancellationToken token)
        {
            // search statistics
            var nodestats = new long[3]; // 0: find, 1: layout, 2: fill

            var m = pieces.Length;
            var taken = new bool[m];
            var stack = new (int i,int d,int n)[m];
            var square = new bool[n,n];
            for (var p=0; p<m; p++) {
                // Console.WriteLine($"*** pivot: p = {p} ({pieces[p].w}x{pieces[p].h})");
                Array.Fill(taken, false);
                taken[p] = true;
                Array.Fill(stack, (-1, -1, -1));
                var dir = 0; // current search direction (0: top edge, 1: right edge, 2: bottom edge, 3: left edge)
                var nfree = n - pieces[p].w; // number of uncovered tiles along search direction
                if (nfree == 0) {
                    dir = 1;
                    nfree = n - pieces[p].h;
                }
                var top = 0; // current search depth
                var tbj = 0; // backjump target
                int i = -1, k; // oriented piece index, piece index
                do {
                    if (token.IsCancellationRequested) {
                        return (null, nodestats);
                    }
                    // find next piece
                    do {
                        i++;
                        k = i >> 1;
                    } while (k < m && taken[k]);
                    // Console.WriteLine($"*** top = {top}, dir = {dir}, i = {i} (k = {k}), nfree = {nfree}");
                    if (k == m) {
                        tbj = top-1;
                        goto backtrack;
                    }
                    Debug.Assert(k < m);
                    nodestats[0]++;
                    // width and height of piece k (oriented i)
                    // note: kw is the dimension along the edge 'dir', kh is perpendicular to the
                    //       edge 'dir', i.e. kw is width for dir=0,2 and height for dir=1,3
                    int kw, kh;
                    if ((i & 0x1) == 0) {
                        (kw, kh) = pieces[k];
                    }
                    else {
                        (kh, kw) = pieces[k];
                    }
                    // note: condition 'k > p' eliminates symmetric solutions
                    if (nfree > kw || (nfree == kw && k > p && (dir != 2 || kh <= n - pieces[p].h))) {
                        // Console.WriteLine($"*** add top = {top,2}, dir = {dir}, i = {i,2} (k = {k,2}), (kw,kh) = ({kw,2},{kh,2}), nfree = {nfree,2}");
                        stack[top++] = (i, dir, nfree);
                        taken[k] = true;
                        i = -1;
                        nfree -= kw;
                        if (nfree == 0) {
                            dir++;
                            if (dir > 3) {
                                goto layout;
                            }
                            nfree = dir < 3 ? n - kh : n - pieces[p].h - kh;
                        }
                        if (nfree == 0) {
                            dir++;
                            if (dir > 3) {
                                goto layout;
                            }
                            nfree = dir < 3 ? n - kw : n - pieces[p].h - kw;
                        }
                    }
                    continue;
                layout:
                    // Console.WriteLine($"*** layout: taken = {String.Join(", ", taken.Select(t => t ? 1 : 0))}");
                    if (!Layout(n, square, p, stack[0..top], pieces, ref tbj, ref nodestats[1])) {
                        goto backtrack;
                    }
                    if (!Fill(n, square, taken, stack, top, pieces, ref nodestats[2])) {
                        tbj = top-1;
                        goto backtrack;
                    }
                    // solution found for piece p
                    var result = RestoreSolution(n, p, stack, top, pieces).ToArray();
                    return (result, nodestats);
                backtrack:
                    if (tbj < 0) {
                        break; // no solution found for piece p, continue search with next piece
                    }
                    Debug.Assert(top > tbj);
                    do {
                        (i, dir, k) = stack[--top];
                        nfree = k;
                        k = i >> 1;
                        taken[k] = false;
                        // Console.WriteLine($"*** del top = {top,2}, dir = {dir}, i = {i,2} (k = {k,2}), nfree = {nfree}");
                    } while (top > tbj);
                } while (true);
            }
            return (null, nodestats);
        }

    }
}
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Mondrian
{
    // Placement strategy 7 -- Corner enumeration
    //
    // Try to place 4 corner pieces, then fill rest of square with
    // top-most-then-left-most strategy.
    //
    // The 4 corner pieces are placed such that checking symmetric solutions
    // is prevented.
    public sealed partial class Placer7
    {
        // enumerate all 4 corner pieces in square domain
        private static IEnumerable<(int nc,(int i,int c)[])> FindCornersSquare(int n, (int w,int h)[] pieces, long[] nodestats)
        {
            var m = pieces.Length;
            var taken = new bool[(m>>1)];
            Array.Fill(taken, false);
            var stack = new (int i,int c)[] { (-1, -1), (-1, -1), (-1, -1), (-1, -1) };
            var top = 0; // current search depth
            int c = 0; // search corner (0: top-left, 1: top-right, 2: bottom-right, 3: bottom-left)
            int i = -1, k, p = -1; // oriented piece index, piece index, pivot piece (on stack[0])
            while (true) {
                // find next piece
                do {
                    i++;
                    k = i >> 1;
                } while (i < m && taken[k]);
                // Console.WriteLine($"*** top = {top}, dir = {dir}, i = {i} (k = {k}), nfree = {nfree}");
                if (i == m) {
                    goto backtrack;
                }
                Debug.Assert(k < (m>>1));
                Debug.Assert(top == 0 || (stack[0].i >> 1) == p);
                nodestats[top]++;
                var (w, h) = pieces[i];
                if (w == h && (i&0x1) == 1) {
                    continue;
                }
                switch (c) {
                case 0: {
                    if ((i & 0x1) != 0) {
                        continue; // eliminate symmetric solutions (transposition)
                    }
                    else {
                        p = k;
                    }
                    break;
                }
                case 1: {
                    if (n-pieces[stack[top-1].i].w < pieces[i].w) {
                        continue; // piece i does not fit in top right corner
                    }
                    break;
                }
                case 2: {
                    if (n-pieces[stack[top-1].i].h < pieces[i].h) {
                        // Note: assuming diagonal pieces (0 and 2) never overlap
                        continue; // piece i does not fit in lower right corner
                    }
                    break;
                }
                case 3: {
                    if (n-pieces[stack[top-1].i].w < w || n-pieces[stack[0].i].h < h) {
                        // Note: assuming diagonal pieces (1 and 3) never overlap
                        continue; // piece i does not fit in lower left corner
                    }
                    break;
                }}
                // Console.WriteLine($"*** corner add top = {top}, i = {i,2} (k = {k,2}), (w,h) = ({pieces[i].w,2},{pieces[i].h,2}), c = {c}");
                stack[top++] = (i, c);
                taken[k] = true;
                c = c switch {
                    0 => pieces[i].w < n ? 1 : 2,
                    1 => pieces[i].h < n ? 2 : 3,
                    2 => pieces[i].w < n ? 3 : 4,
                    3 => 4,
                    _ => throw new InvalidOperationException()
                };
                i = (p<<1) + 1;  // eliminate symmetric solutions
                if (c <= 3) {
                    continue;
                }
                yield return (top, stack);
                // fall through to backtrack
            backtrack:
                if (top == 0) {
                    break; // no solution found for piece p, continue search with next piece
                }
                (i, c) = stack[--top];
                k = i >> 1;
                taken[k] = false;
                // Console.WriteLine($"*** corner del top = {top}, i = {i,2} (k = {k,2}), c = {c}");
            }
       }

        // place full-width piece at bottom edge and enumerate 4 corner pieces in rectangualr domain
        private static IEnumerable<(int nc,(int i,int c)[])> FindCornersRect(int n, (int w,int h)[] pieces, long[] nodestats)
        {
            var m = pieces.Length;
            var taken = new bool[m>>1];
            Array.Fill(taken, false);
            var stack = new (int i,int c)[] { (-1, -1), (-1, -1), (-1, -1), (-1, -1), (-1, -1) };

            int i = -1, k, p = -1; // oriented piece index, piece index, pivot piece (on stack[0])

            // place full-width piece at bottom
            i = pieces.TakeWhile(p => p.w != n).Count();
            k = i >> 1;
            stack[0] = (i, -1);
            taken[k] = true;
            // rectangle dimensions
            var nw = n;
            var nh = n - pieces[i].h;

            // enumerate 4 corner pieces
            var top = 1; // current search depth
            int c = 0; // search corner (0: top-left, 1: top-right, 2: bottom-right, 3: bottom-left)
            i = -1;
            while (true) {
                // find next piece
                do {
                    i++;
                    k = i >> 1;
                } while (i < m && taken[k]);
                // Console.WriteLine($"*** top = {top}, dir = {dir}, i = {i} (k = {k}), nfree = {nfree}");
                if (i == m) {
                    goto backtrack;
                }
                Debug.Assert(k < (m>>1));
                Debug.Assert(top == 1 || (stack[1].i >> 1) == p);
                switch (c) {
                case 0: {
                    Debug.Assert(nh >= pieces[i].h);
                    if (nh < pieces[i].h) {
                        continue; // piece i does not fit in top left corner
                    }
                    // if (nh == pieces[i].h) {
                    //     continue; // eliminate symmetric solutions
                    // }
                    p = k;
                    break;
                }
                case 1: {
                    Debug.Assert(nh >= pieces[i].h);
                    if (nw-pieces[stack[top-1].i].w < pieces[i].w || nh < pieces[i].h) {
                        continue; // piece i does not fit in top right corner
                    }
                    break;
                }
                case 2: {
                    if (nh-pieces[stack[top-1].i].h < pieces[i].h) {
                        // Note: assuming diagonal pieces (0 and 2) never overlap
                        continue; // piece i does not fit in lower right corner
                    }
                    break;
                }
                case 3: {
                    var (w, h) = pieces[i];
                    if (nw-pieces[stack[top-1].i].w < w || nh-pieces[stack[1].i].h < h) {
                        // Note: assuming diagonal pieces (1 and 3) never overlap
                        continue; // piece i does not fit in lower left corner
                    }
                    break;
                }}
                nodestats[top]++;
                // Console.WriteLine($"*** corner add top = {top}, i = {i,2} (k = {k,2}), (w,h) = ({pieces[i].w,2},{pieces[i].h,2}), c = {c}");
                stack[top++] = (i, c);
                taken[k] = true;
                c = c switch {
                    0 => pieces[i].w < nw ? 1 : 2,
                    1 => pieces[i].h < nh ? 2 : 3,
                    2 => pieces[i].w < nw ? 3 : 4,
                    3 => 4,
                    _ => throw new InvalidOperationException($"Invalid corner value: c = {c}")
                };
                i = (p<<1) + 1;  // eliminate symmetric solutions
                if (c < 3 || (c == 3 && pieces[stack[1].i].h < nh)) {
                    continue;
                }
                yield return (top, stack);
                // fall through to backtrack
            backtrack:
                if (top == 1) {
                    break; // no solution found for piece p, continue search with next piece
                }
                (i, c) = stack[--top];
                k = i >> 1;
                taken[k] = false;
                // Console.WriteLine($"*** corner del top = {top}, i = {i,2} (k = {k,2}), c = {c}");
            }
       }
        
        private static IEnumerable<(int nc,(int i,int c)[])> FindCorners(int n, (int w,int h)[] pieces, long[] nodestats)
        {
            if (pieces.Any(p => p.w == n)) {
                return FindCornersRect(n, pieces, nodestats);
            }
            else {
                return FindCornersSquare(n, pieces, nodestats);
            }
        }
   }
}
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Threading;

namespace Mondrian
{
    // Placement strategy 8
    //
    // Phase 2: Edge placement
    public sealed unsafe partial class Placer8
    {
        public bool FindEdges()
        {
            var pieces = this.pieces;
            (int i,int dir,int pos)[] stack = this.stack;
            var square = this.horz_square;
            var nodestats = this.nodestats;
            var token = this.token;

            var nw = this.nw;
            var nh = this.nh;

            var ncorners = this.ncorners;

            var m = pieces.Length;

            // if (ncorners != 5 || stack[1].i != 0 || stack[2].i != 9 || stack[3].i != 14 || stack[4].i != 6) {
            //     Console.WriteLine($"*** corner skipping: {String.Join(", ", corners.Take(nc).Select(s => $"{s.i} ({pieces[s.i].w} x {pieces[s.i].h})"))}");
            //     return false;
            // }
            // Console.WriteLine($"*** corner checking: {String.Join(", ", corners.Take(nc).Select(s => $"{s.i} ({pieces[s.i].w} x {pieces[s.i].h})"))}");

            var pos = (Span<int>)stackalloc int[] { -1, -1, -1, -1 };  // current placement position along edge 'dir'
            var nfree = (Span<int>)stackalloc int[] { nw, nh, nw, nh }; // current number of free tiles along edge 'dir'
            var pc = nh == nw ? 0 : 1; // stack position of pivot corner (top-left piece)

            // compute edge gaps and place corner pieces
            Array.Fill(this.mem_horz_square, (byte)0);
            for (var tt=pc; tt<ncorners; tt++) {
                var (ti, c, _) = stack[tt];
                var (tw, th) = pieces[stack[tt].i];
                switch (c) {
                case -1: {
                    var mask = ((1UL << nw) - 1UL);
                    for (var sy=nw-th; sy<nw; sy++) { // note: use 'nw' for full height
                        square[sy] |= mask;
                    }
                    break;
                }
                case 0: {
                    pos[0] = tw;
                    // pos[1] = th; // corner 0 is never full-width
                    nfree[0] -= tw;
                    nfree[3] -= th;
                    var mask = ((1UL << tw) - 1UL);
                    for (var sy=0; sy<th; sy++) {
                        square[sy] |= mask;
                    }
                    break;
                }
                case 1: {
                    pos[1] = th;
                    pos[2] = nw-tw-1; // will be overwritten by corner 2 (if exists)
                    nfree[0] -= tw;
                    nfree[1] -= th;
                    var mask = ((1UL << tw) - 1UL) << nw-tw;
                    for (var sy=0; sy<th; sy++) {
                        square[sy] |= mask;
                    }
                    break;
                }
                case 2: {
                    pos[2] = nw-tw-1;
                    // pos[3] = nh-th-1; // corner 2 is never full-width
                    nfree[1] -= th;
                    nfree[2] -= tw;
                    var mask = ((1UL << tw) - 1UL) << nw-tw;
                    for (var sy=nh-th; sy<nh; sy++) {
                        square[sy] |= mask;
                    }
                    break;
                }
                case 3: {
                    pos[3] = nh-th-1;
                    nfree[2] -= tw;
                    nfree[3] -= th;
                    var mask = ((1UL << tw) - 1UL);
                    for (var sy=nh-th; sy<nh; sy++) {
                        square[sy] |= mask;
                    }
                    break;
                }
                default:
                    throw new InvalidOperationException($"Invalid corner value: c = {c}");
                }
            }
            // Print(ncorners, (-1, -1));

            var dir = 0; // current search direction (0: top edge, 1: right edge, 2: bottom edge, 3: left edge)
            var top = ncorners; // current search position
            int w, h, l; // piece width, height, length along current edge 'dir'
            int i = -1, k; // oriented piece index, piece index
            while (true) {
                if (token.IsCancellationRequested) {
                    return false;
                }

                while (dir <= 3 && nfree[dir] == 0) {
                    dir++;
                    Debug.Assert(dir == 4 || nfree[dir] >= 0);
                }
                if (dir == 4) {
                    goto fill;
                }
                Debug.Assert(pos[dir] >= 0);
                Debug.Assert(((dir&0x1)==0) && pos[dir] < nw || ((dir&0x1)==1) && pos[dir] < nh);


            next:
                // find next piece
                i++;
                k = i >> 1;
                k = (int)Bmi1.X64.TrailingZeroCount(~(this.taken | ((1UL<<k)-1UL)));
                i = Math.Max(i, k<<1);
                if (i == m) {
                    goto backtrack;
                }
                Debug.Assert(i < m && k < (m>>1));
                // Console.WriteLine($"*** top = {top}, dir = {dir}, i = {i} (k = {k}), nfree = {nfree}");
                nodestats[top]++;
                // if (nodestats.Sum() > 2200) {
                //     return false;
                // }
                (w, h) = pieces[i];

                if (w == h && (i&0x1) == 1) {
                    goto next; // do not check transpose of square pieces (continue w/o recomputing dir)
                }

                l = (dir & 0x1) == 0 ? w : h;
                if (nfree[dir] < l) {
                    goto next; // piece i does not fit (continue w/o recomputing dir)
                }


                switch (dir) {
                case 0: {
                    var mask = ((1UL << w) - 1UL) << pos[dir];
                    // check for intersections
                    if ((square[h-1] & mask) != 0UL) {
                        // Console.WriteLine($"*** layout: intersection at y = {h-1}");
                        // Print(n, square, tx, ty);
                        goto next; // continue w/o recomputing dir
                    }
                    // mark occupied area
                    for (var sy=0; sy<h; sy++) {
                        square[sy] |= mask;
                    }
                    // update search state
                    stack[top++] = (i, dir, pos[dir]);
                    this.taken |= 1UL << k;
                    // advance placement position
                    pos[dir] += l;
                    nfree[dir] -= l;
                    break;
                }
                case 1: {
                    var mask = ((1UL << w) - 1UL) << nw-w;
                    // check for intersections
                    var py = pos[dir];
                    for (var sy=py+h-1; sy>=py; sy--) {
                        if ((square[sy] & mask) != 0UL) {
                            // Console.WriteLine($"*** layout: intersection at y = {sy}");
                            // Print(n, square, tx, ty);
                            goto next; // continue w/o recomputing dir
                        }
                    }
                    // mark occupied area
                    for (var sy=py+h-1; sy>=py; sy--) {
                        square[sy] |= mask;
                    }
                    // update search state
                    stack[top++] = (i, dir, py);
                    this.taken |= 1UL << k;
                    // advance placement position
                    pos[dir] += l;
                    nfree[dir] -= l;
                    break;
                }
                case 2: {
                    var mask = ((1UL << w) - 1UL) << pos[dir]-w+1;
                    // check for intersections
                    for (var sy=nh-h; sy<nh; sy++) {
                        if ((square[sy] & mask) != 0UL) {
                            // Console.WriteLine($"*** layout: intersection at y = {sy}");
                            // Print(n, square, tx, ty);
                            goto next; // continue w/o recomputing dir
                        }
                    }
                    // mark occupied area
                    for (var sy=nh-h; sy<nh; sy++) {
                        square[sy] |= mask;
                    }
                    // update search state
                    stack[top++] = (i, dir, pos[dir]);
                    this.taken |= 1UL << k;
                    // advance placement position
                    pos[dir] -= l;
                    nfree[dir] -= l;
                    break;
                }
                case 3: {
                    var mask = (1UL << w) - 1UL;
                    // check for intersections
                    var py = pos[dir];
                    for (var sy=py-h+1; sy<=py; sy++) {
                        if ((square[sy] & mask) != 0UL) {
                            // Console.WriteLine($"*** layout: intersection at y = {sy}");
                            // Print(n, square, tx, ty);
                            goto next; // continue w/o recomputing dir
                        }
                    }
                    // mark occupied area
                    for (var sy=py-h+1; sy<=py; sy++) {
                        square[sy] |= mask;
                    }
                    // update search state
                    stack[top++] = (i, dir, py);
                    this.taken |= 1UL << k;
                    // advance placement position
                    pos[dir] -= l;
                    nfree[dir] -= l;
                    break;
                }
                default:
                    throw new InvalidOperationException();
                }
                // Console.WriteLine($"*** edges: add top = {top-1,2}, dir = {dir}, i = {stack[top-1].i,2} (k = {stack[top-1].i>>1,2}), (w,h) = ({w,2},{h,2}), pos = {stack[top-1].Item2.pos,2}, nfree = {stack[top-1].Item2.nfree,2}");
                // Print(top, dir switch { 0 => (pos, 0), 1 => (nw-1, pos), 2 => (pos, nw-1), 3 => (0, pos) });
                i = -1;
                continue;
            fill:
                // Print(top, (-1, -1));
                this.nedges = top;
                if (FindInterior()) {
                    return true;
                }
                this.nedges = stack.Length;
                // fall through to backtrack
            backtrack:
                if (top == ncorners) {
                    break; // no solution found
                }
                (i, dir, l) = stack[--top]; // reuse l for current pos
                pos[dir] = l;
                k = i >> 1;
                this.taken &= ~(1UL << k);
                (w, h) = pieces[i];
                l = (dir & 0x1) == 0 ? w : h;
                nfree[dir] += l;


                switch (dir) {
                case 0: {
                    var mask = ~(((1UL << w) - 1UL) << pos[dir]);
                    for (var sy=0; sy<h; sy++) {
                        square[sy] &= mask;
                    }
                    break;
                }
                case 1: {
                    var mask = ~(((1UL << w) - 1UL) << nw-w);
                    var py = pos[dir];
                    for (var sy=py+h-1; sy>=py; sy--) {
                        square[sy] &= mask;
                    }
                    break;
                }
                case 2: {
                    var mask = ~(((1UL << w) - 1UL) << pos[dir]-w+1);
                    for (var sy=nh-h; sy<nh; sy++) {
                        square[sy] &= mask;
                    }
                    break;
                }
                case 3: {
                    var mask = ~((1UL << w) - 1UL);
                    var py = pos[dir];
                    for (var sy=py-h+1; sy<=py; sy++) {
                        square[sy] &= mask;
                    }
                    break;
                }
                default:
                    throw new InvalidOperationException();
                }


                // Console.WriteLine($"*** del top = {top,2}, dir = {dir}, i = {i,2} (k = {k,2}), nfree = {nfree}");
            }                
            return false;
        }

        private IEnumerable<(int x,int y,int w,int h,int i)> ReconstructEdges()
        {
            var pieces = this.pieces;
            var stack = this.stack;

            var nw = this.nw;
            var nh = this.nh;

            var ncorners = this.ncorners;
            var nedges = this.nedges;

            var m = stack.Length;

            var top = ncorners;
            while (top < nedges) {
                var (i, dir, pos) = stack[top++];
                if (pos == unused) {
                    break;
                }
                var (w, h) = pieces[i];
                switch (dir) {
                case 0: {
                    yield return (pos, 0, w, h, i>>1);
                    break;
                }
                case 1: {
                    yield return (nw-w, pos, w, h, i>>1);
                    break;
                }
                case 2: {
                    yield return (pos-w+1, nh-h, w, h, i>>1);
                    break;
                }
                case 3: {
                    yield return (0, pos-h+1, w, h, i>>1);
                    break;
                }
                default:
                    throw new InvalidOperationException($"Invalid edge value: dir = {dir}");
                }
            }
        }
    }
}
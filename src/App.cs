using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using NDesk.Options;

namespace Mondrian
{
    class App
    {
        private static void TestFinder()
        {
            foreach (var candidate in Finder.FindCandidatesSF(11, 6))
            {
                Console.WriteLine($"*** candidate 11/6: {String.Join(", ", candidate.Select(s => $"({s.w} x {s.h})"))}");
            }
        }

        private static void TestPlacer11()
        {
            // A size-11 square can be divided into 3 X 4, 2 X 6, 2 X 7, 3 X 5, 4 X 4, 2 X 8, 2 X 9, and 3 X 6 rectangles
            var n = 11;
            var pieces = new (int,int)[] {
                (4, 3),
                (4, 4),
                (5, 3),
                (6, 2),
                (6, 3),
                (7, 2),
                (8, 2),
                (9, 2),
            };
            var (placement, nnodes) = Placer.Find(n, pieces);
            // if (placement != null) {
            //     PrintSolution(n, placement);
            // }
        }

        private static void TestPlacer14()
        {
            var n = 14;
            var pieces = new (int,int)[] {
                (6,  5),
                (6,  6),
                (7,  5),
                (8,  4),
                (10, 3),
                (11, 3),
            };
            Placer.Find(n, pieces);
            var (placement, nnodes) = Placer.Find(n, pieces);
            // if (placement != null) {
            //     PrintSolution(n, placement);
            // }
        }

        private delegate ((int x,int y,int w,int h,int i)[]?, long[] nodestats) PlacerFunc(int n, (int w,int h)[] candidates, CancellationToken token);

        private static void PrintUsage(OptionSet options)
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("");
            Console.WriteLine("dotnet run [options] N V");
            Console.WriteLine("dotnet run --help");
            Console.WriteLine("");
            Console.WriteLine("List of  options:");
            options.WriteOptionDescriptions(System.Console.Out);
            Console.WriteLine("");
        }

        private static void PrintSolution(int n, (int x,int y,int w,int h,int i)[] placement) {
            const string symbols = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var L = symbols.Length;
            if (placement.Length > L) {
                Console.WriteLine("                      WARNING: symbols are not unique");
            }
            for (var y=0; y<n; y++) {
                Console.Write("                      ");
                for (var x=0; x<n; x++) {
                    for (var k=0; k<placement.Length; k++) {
                        var (tx, ty, tw, th, ti) = placement[k];
                        if (tx <= x && x < tx+tw && ty <= y && y < ty+th) {
                            Console.Write(symbols[ti%L]);
                            break;
                        }
                    }
                }
                Console.WriteLine();
            }
        }

        private static void PrintStatistics(TimeSpan dt, int ncandidates, PlacerStats stats) {
            Console.WriteLine("                      Solution statistics");
            Console.WriteLine($"                       - time         {24*dt.Days+dt.Hours}:{dt.Minutes:00}:{dt.Seconds:00}.{dt.Milliseconds:000}");
            Console.WriteLine($"                       - candidates   {ncandidates}");
            Console.WriteLine($"                       - total nodes  {stats.NumNodes}");
            for (var i=0; i<stats.MaxDepth; i++) {
            Console.WriteLine($"                                  {i,2}  {stats.NodeHist[i]}");
            }
        }

        // optimal values (from https://oeis.org/A276523):
        //
        //                   n = 20, v =  9     n = 40, v = 12
        //                   n = 21, v =  9     n = 41, v = 13
        //                   n = 22, v =  9     n = 42, v = 12
        // n =  3, v = 2     n = 23, v =  8     n = 43, v = 12
        // n =  4, v = 4     n = 24, v =  9     n = 44, v = 12
        // n =  5, v = 4     n = 25, v = 10     n = 45, v = 13
        // n =  6, v = 5     n = 26, v =  9     n = 46, v = 13
        // n =  7, v = 5     n = 27, v = 10     n = 47, v = 12
        // n =  8, v = 6     n = 28, v =  9     n = 48, v = 14
        // n =  9, v = 6     n = 29, v =  9     n = 49, v = 12
        // n = 10, v = 8     n = 30, v = 11     n = 50, v = 13
        // n = 11, v = 6     n = 31, v = 11     n = 51, v = 14
        // n = 12, v = 7     n = 32, v = 10     n = 52, v = 13
        // n = 13, v = 8     n = 33, v = 12     n = 53, v = 14
        // n = 14, v = 6     n = 34, v = 12     n = 54, v = 15
        // n = 15, v = 8     n = 35, v = 11     n = 55, v = 14
        // n = 16, v = 8     n = 36, v = 12     n = 56, v = 14
        // n = 17, v = 8     n = 37, v = 11     n = 57, v = 15
        // n = 18, v = 8     n = 38, v = 10
        // n = 19, v = 8     n = 39, v = 11

        public static void Main(string[] args)
        {
            // TestFinder();
            // TestPlacer11();
            // TestPlacer14();

            // init options

            OptionSet options = new OptionSet();
            var opt_largest = false;
            var opt_verbose = 0;
            var opt_threads = 1;
            var opt_placer = (PlacerFunc)Placer5.Find;
            var opt_x_skip = 0;
            var opt_x_take = Int32.MaxValue;
            var opt_help = false;

            options.Add("l|largest", "Enumerate candidates largest first (default: smallest first).", delegate (string val) { opt_largest = true; });
            options.Add("v|verbose", "Increase verbosity.", delegate (string val) { opt_verbose++; });
            options.Add("t|threads=", "Number of threads.", delegate (string val) {
                if (!Int32.TryParse(val, out opt_threads) || opt_threads <= 0) {
                    throw new OptionException($"Invalid thread count: {val}", "threads");
                }
            });
            options.Add("placer=", "Placement algorithm.", delegate (string val) {
                opt_placer = val switch {
                    "3" => Placer3.Find,
                    "4" => Placer4.Find,
                    "5" => Placer5.Find,
                    "5L" => Placer5L.Find,
                    "6L" => Placer6L.Find,
                    "7" => Placer7.Find,
                    "7LI" => Placer7LI.Find,
                    "8" => Placer8.Find,
                    _ => throw new OptionException($"Invalid placer algorithm: {val}", "placer")
                };
            });
            options.Add("x-skip=", "Skip the first n candidates.", delegate (string val) {
                if (!Int32.TryParse(val, out opt_x_skip) || opt_x_skip < 0) {
                    throw new OptionException($"Invalid skip count: {val}", "x-skip");
                }
            });
            options.Add("x-take=", "Only take n candidates.", delegate (string val) {
                if (!Int32.TryParse(val, out opt_x_take) || opt_x_take < 0) {
                    throw new OptionException($"Invalid take count: {val}", "x-take");
                }
            });
            options.Add("h|help", "Show usage information.", delegate (string val) { opt_help = true; });

            if (args.Length == 0) {
                PrintUsage(options);
                return;
            }

            // parse options

            List<string> argx;
            try {
                argx = options.Parse(args);
            }
            catch (OptionException ex) {
                Console.WriteLine($"Error parsing options: {ex.Message}");
                PrintUsage(options);
                return;
            }
            
            if (opt_help) {
                PrintUsage(options);
                return;
            }

            if (argx.Count != 2) {
                Console.WriteLine("Error: Invalid problem specification.");
                PrintUsage(options);
                return;
            }

            if (!Int32.TryParse(argx[0], out var n)) {
                Console.WriteLine($"Error: Invalid problem size argument: {argx[0]}.");
                PrintUsage(options);
                return;
            }

            if (!Int32.TryParse(argx[1], out var v)) {
                Console.WriteLine($"Error: Invalid gap size argument: {argx[1]}.");
                PrintUsage(options);
                return;
            }

            // run solver

            Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss}] Start search: grid {n}x{n}, gap {v}");

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            var ncandidates = 0;
            var stats = new PlacerStats();

            var candidates = (opt_largest ? Finder.FindCandidatesLF(n, v) : Finder.FindCandidatesSF(n, v)).Skip(opt_x_skip).Take(opt_x_take).GetEnumerator();
            var cts = new CancellationTokenSource();
            var threads = new Thread[opt_threads];
            var pieces = ((int w,int h)[]?)null;
            var placement = ((int x,int y,int w,int h,int i)[]?)null;
            for (var i=0; i<opt_threads; i++) {
                threads[i] = new Thread((ThreadStart)delegate {
                    var token = cts.Token;
                    while (!token.IsCancellationRequested) {
                        (int w,int h)[] tpieces;
                        lock (candidates) {
                            if (!candidates.MoveNext()) {
                                return;
                            }
                            tpieces = candidates.Current;
                            ncandidates++;
                        }
                        Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss}] Candidate ({tpieces.Length}, {tpieces.Select(s => s.w*s.h).Min()}-{tpieces.Select(s => s.w*s.h).Max()}): {String.Join(", ", tpieces.Select(s => $"({s.w} x {s.h})"))}");
                        if (!Filter.Check(n, tpieces, out int c)) {
                            Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss}] Condidate filtered due to conflicting piece {c} ({tpieces[c].w} x {tpieces[c].h}).");
                            continue;
                        }
                        var (tplacement, tnodes) = opt_placer(n, tpieces, token);
                        stats.Update(tnodes);
                        if (tplacement != null) {
                            if (Interlocked.CompareExchange(ref placement, tplacement, null) == null) {
                                pieces = tpieces;
                                stopwatch.Stop();
                                cts.Cancel();
                                return;
                            }
                        }
                    }
                });
                threads[i].Start();
            }

            foreach (var thread in threads) {
                thread.Join();
            }

            if (pieces != null && placement != null) {
                Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss}] Solution found (grid {n}x{n}, gap {v})");
                PrintSolution(n, placement);
                PrintStatistics(stopwatch.Elapsed, ncandidates, stats);
            }
            else {
                stopwatch.Stop();
                Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss}] No solution found (grid {n}x{n}, gap {v})");
                PrintStatistics(stopwatch.Elapsed, ncandidates, stats);
            }

            // foreach (var pieces in ) {
            //     ncandidates++;
            //     Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss}] Candidate {String.Join(", ", pieces.Select(s => $"({s.w} x {s.h})"))}");
            //     var (placement, tnodes) = Placer3.Find(n, pieces);
            //     nnodes += tnodes;
            //     if (placement != null) {
            //         stopwatch.Stop();
            //         Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss}] Solution found (grid {n}x{n}, gap {v})");
            //         PrintSolution(n, pieces, placement);
            //         PrintStatistics(stopwatch.Elapsed, ncandidates, nnodes);
            //         break;
            //     }
            // }
            // if (stopwatch.IsRunning) {
            //     stopwatch.Stop();
            //     Console.WriteLine($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss}] No solution found (grid {n}x{n}, gap {v})");
            //     PrintStatistics(stopwatch.Elapsed, ncandidates, nnodes);
            // }

        }
    }
}
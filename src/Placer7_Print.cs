using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Mondrian
{
    // Placement strategy 7 -- Printing
    public sealed partial class Placer7
    {
        private static void Print(int n, (int w,int h)[] pieces, (int i,int c,int x,int y,int l)[] stack, int top, bool[] taken, int cc, int cx, int cy, int cw, int[][] next)
        {
            const string symbols = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Console.WriteLine($"*** stack: [{String.Join(", ", stack[0..top].Select(s=>symbols[s.i>>1]))}]");
            Console.WriteLine($"*** taken: [{String.Join(", ", taken.Select(ti => ti ? "1" : "0"))}]");
            Console.WriteLine($"*** cc = {cc}, cx = {cx}, cy = {cy}, cw = {cw}");
            for (var y=0; y<n; y++) {
                Console.Write("*** ");
                for (var x=0; x<n; x++) {
                    var t = 0;
                    for (; t<top; t++) {
                        int ti, tc, tx, ty, tl, tw, th;
                        (ti, tc, tx, ty, tl) = stack[t];
                        (tw, th) = pieces[ti];
                        if (1 <= tc && tc <= 2) {
                            tx = tx - tw + 1;
                        }
                        if (2 <= tc && tc <= 3) {
                            ty = ty - th + 1;
                        }
                        if (tx <= x && x < tx+tw && ty <= y && y < ty+th) {
                            Console.Write(symbols[ti>>1]);
                            break;
                        }
                    }
                    if (t == top) {
                        if (x==cx && y==cy) {
                            Console.Write("*");
                        }
                        else if (next[0][x] == y || next[1][x] == y) {
                            Console.Write("~");
                        }
                        else {
                            Console.Write(".");
                        }
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
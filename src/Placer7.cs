using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Mondrian
{
    // Placement strategy 7
    //
    // Try to place 4 corner pieces, then fill rest of square with a
    // 'low plateau' placement strategy.
    //
    // The 4 corner pieces are placed such that checking symmetric solutions
    // is prevented.
    //
    // There are currently 3 'low plateau' placement strategies implemented:
    //  - minimum x-length
    //  - minimum area
    //  - minimum x-length + minimum y-length
    public sealed partial class Placer7
    {
        private const int unused = -1;

        // create array of oriented (normal/transposed) candidate pieces
        private static (int w,int h)[] CreateOrientedPieces((int w,int h)[] candidates) {
            var m = 2*candidates.Length;
            var pieces = new (int w,int h)[m];
            for (var i=0; i<m; i++) {
                int w, h;
                if ((i & 0x1) == 0) {
                    (w, h) = candidates[i>>1];
                }
                else {
                    (h, w) = candidates[i>>1];
                }
                pieces[i] = (w, h);
            }
            return pieces;
        }

        // find next placement location: minimum x-length
        private static (int cx,int cy,int cw) NextP1(int n, int[][] next)
        {
            int cx = n, cy = n;
            var cw = Int32.MaxValue;
            var (txb, tyb) = next[0][0] <= next[1][0] ? (0, next[0][0]) : (n, n);
            for (int tx=1; tx<n; tx++) {
                var n0x = next[0][tx];
                var n1x = next[1][tx];
                if (n0x > n1x) {
                    if (txb < n && tx-txb < cw) {
                        cx = txb;
                        cy = tyb;
                        cw = tx-txb;
                    }
                    txb = n;
                    tyb = n;
                }
                else if (n0x > tyb) {
                    if (txb < n && tx-txb < cw) {
                        cx = txb;
                        cy = tyb;
                        cw = tx-txb;
                    }
                    txb = n;
                    tyb = n0x;
                }
                else if (n0x < tyb) {
                    txb = tx;
                    tyb = n0x;
                }
            }
            if (cw == Int32.MaxValue) {
                Debug.Assert(txb < n);
                        cx = txb;
                        cy = tyb;
                cw = n - txb;
            }
            return (cx, cy, cw);
        }

        // find next placement location: minimum area
        private static (int cx,int cy,int cw) NextP2(int n, int[][] next)
        {
            int cx = n, cy = n;
            var cw = Int32.MaxValue;
            var (txb, tyb) = next[0][0] <= next[1][0] ? (0, next[0][0]) : (n, n);
            var amin = Int32.MaxValue;
            var acur = Math.Max(next[1][0] - next[0][0], 0);
            for (int tx=1; tx<n; tx++) {
                var n0x = next[0][tx];
                var n1x = next[1][tx];
                if (n0x > n1x) {
                    if (txb < n && acur < amin) {
                        cx = txb;
                        cy = tyb;
                        cw = tx-txb;
                        amin = acur;
                    }
                    txb = n;
                    tyb = n;
                    acur = 0;
                }
                else if (n0x > tyb) {
                    if (txb < n && acur < amin) {
                        cx = txb;
                        cy = tyb;
                        cw = tx-txb;
                        amin = acur;
                    }
                    txb = n;
                    tyb = n0x;
                    acur = 0;
                }
                else if (n0x < tyb) {
                    txb = tx;
                    tyb = n0x;
                    acur = n1x - n0x;
                }
                else {
                    acur += n1x - n0x;
                }
            }
            if (cw == Int32.MaxValue) {
                Debug.Assert(txb < n);
                cx = txb;
                cy = tyb;
                cw = n - txb;
            }
            return (cx, cy, cw);
        }

        // find next placement location: minimum x-length + minimum y-length
        private static (int cc,int cx,int cy,int cw) NextP3(int n, int[][] next)
        {
            int cx = n, cy = n;
            var cw = 0;
            var lwh = Int32.MaxValue;
            var (txb, tyb) = next[0][0] <= next[1][0] ? (0, next[0][0]) : (n, n);
            var lh = Math.Max(next[1][0] - next[0][0], 0);
            for (int tx=1; tx<n; tx++) {
                var n0x = next[0][tx];
                var n1x = next[1][tx];
                if (n0x > n1x) {
                    if (txb < n && lh+(tx-txb) < lwh) {
                        cx = txb;
                        cy = tyb;
                        cw = tx-txb;
                        lwh = cw + lh;
                    }
                    txb = n;
                    tyb = n;
                    lh = 0;
                }
                else if (n0x > tyb) {
                    if (txb < n && lh+(tx-txb) < lwh) {
                        cx = txb;
                        cy = tyb;
                        cw = tx-txb;
                        lwh = cw + lh;
                    }
                    txb = n;
                    tyb = n0x;
                    lh = 0;
                }
                else if (n0x < tyb) {
                    txb = tx;
                    tyb = n0x;
                    lh = n1x - n0x;
                }
            }
            if (lwh == Int32.MaxValue) {
                Debug.Assert(txb < n);
                cx = txb;
                cy = tyb;
                cw = n - txb;
            }
            return (0, cx, cy, cw);
        }

        // find next placement location: minimum x-length + minimum y-length
        private static (int cc,int cx,int cy,int cw) NextP4(int n, int[][] next)
        {
            var cc = -1;
            int cx = -1, cy = n;
            var cw = 0;
            var vmin = Int32.MaxValue;
            var w0 = n<<1;
            var w1 = n<<1;
            int x0 = -1, y0 = n;
            int x1 = -1, y1 = -1;
            int h0 = n;
            int h1 = -1;
            int hp = -1;
            for (int tx=0; tx<n; tx++) {
                var n0x = next[0][tx];
                var n1x = next[1][tx];
                if (n0x > n1x) {
                    n0x = n;
                    n1x = -1;
                }
                if (n0x > y0) {
                    var vcur = w0 + Math.Min(h0, hp);
                    if (vcur < vmin) {
                        Debug.Assert(0 <= x0 && x0 < n/*, $"0 <= {vx} < {n} (tx={tx}, n0x={n0x}, vy={vy})"*/);
                        Debug.Assert(y0 == next[0][x0]);
                        vmin = vcur;
                        cc = h0 <= hp ? 0 : 1;
                        cx = h0 <= hp ? x0 : tx-1;
                        cy = y0;
                        cw = tx - x0;
                    }
                    x0 = -1;
                    y0 = n0x;
                    w0 = n<<1;
                }
                else if (n0x < y0) {
                        x0 = tx;
                        y0 = n0x;
                        w0 = 0;
                        h0 = n1x - n0x;
                }
                if (n1x < y1) {
                    var vcur = w1 + Math.Min(h1, hp);
                    if (vcur < vmin) {
                        Debug.Assert(0 <= x1 && x1 < n/*, $"0 <= {vx} < {n} (tx={tx}, n0x={n0x}, vy={vy})"*/);
                        Debug.Assert(y1 == next[1][x1]);
                        vmin = vcur;
                        cc = h1 <= hp ? 3 : 2;
                        cx = h1 <= hp ? x1 : tx-1;
                        cy = y1;
                        cw = tx - x1;
                    }
                    x1 = -1;
                    y1 = n1x;
                    w1 = n<<1;
                }
                else if (n1x > y1) {
                        x1 = tx;
                        y1 = n1x;
                        w1 = 0;
                        h1 = n1x - n0x;
                }
                w0++;
                w1++;
                hp = n1x - n0x;
            }
            if (vmin == Int32.MaxValue) {
                Debug.Assert(0 <= x0 && x0 < n);
                Debug.Assert(y0 == next[0][x0]);
                if (w0 + Math.Min(h0, hp) <= w1 + Math.Min(h1, hp)) {
                    cc = h0 <= hp ? 0 : 1;
                    cx = h0 <= hp ? x0 : n-1;
                    cy = y0;
                    cw = n - x0;
                }
                else {
                    cc = h1 <= hp ? 3 : 2;
                    cx = h1 <= hp ? x1 : n-1;
                    cy = y1;
                    cw = n - x1;
                }
            }
            return (cc, cx, cy, cw);
        }

        public static ((int x,int y,int w,int h,int i)[]?, long[] nodestats) Find(int n, (int w,int h)[] candidates, CancellationToken token)
        {
            var m = candidates.Length;

            // search statistics
            var nodestats = new long[m];

            // oriented pieces
            var pieces = CreateOrientedPieces(candidates);

            // search state
            var next = new int[][] {
                new int[n], // array of next y-placement position [top] (for all x positions)
                new int[n]  // array of next y-placement position [bottom] (for all x positions)
            };
            var taken = new bool[m];
            var stack = new (int i,int c,int x,int y,int l)[m];

            foreach (var (nc, corners) in FindCorners(n, pieces, nodestats)) {

                // if (nc != 5 || corners[0].i != 20 || corners[1].i != 4 || corners[2].i != 17 || corners[3].i != 18 || corners[4].i != 15) {
                //     Console.WriteLine($"*** corner skipping: {String.Join(", ", corners.Take(nc).Select(s => $"{s.i} ({pieces[s.i].w} x {pieces[s.i].h})"))}");
                //     continue;
                // }
                // Console.WriteLine($"*** corner checking: {String.Join(", ", corners.Take(nc).Select(s => $"{s.i} ({pieces[s.i].w} x {pieces[s.i].h})"))}");

                Array.Fill(next[0], 0);
                Array.Fill(next[1], n-1);
                Array.Fill(taken, false);
                // Array.Fill(stack, (-1, (-1, -1), -1));
                var top = 0; // current search position
                var cc = -1; // corner type (0: top left, 1: top right, 2: bottom right, 3: bottom left)
                int cx, cy; // current placement positions
                var cw = 0; // corner 'width' (first occupied tile on line segment cx,..,cx+cw)
                int i = -1, k; // oriented piece index, piece index
                int w, h; // piece width, height
                for (var t=0; t<nc; t++) {
                    i = corners[t].i;
                    k = i >> 1;
                    (w, h) = pieces[i];
                    taken[k] = true;
                    switch (corners[t].c) {
                    case -1:
                        // full-width piece at bottom edge
                        stack[top++] = (i, 0, 0, n-h, unused);
                        for (var x=0; x<w; x++) {
                            next[1][x] -= h;
                        }
                        break;
                    case 0:
                        stack[top++] = (i, 0, 0, 0, unused);
                        for (var x=0; x<w; x++) {
                            next[0][x] += h;
                        }
                        break;
                    case 1:
                        stack[top++] = (i, 0, n-w, 0, unused);
                        for (var x=n-w; x<n; x++) {
                            next[0][x] += h;
                        }
                        break;
                    case 2:
                        stack[top++] = (i, 0, n-w, next[1][n-1]+1-h, unused);
                        for (var x=n-w; x<n; x++) {
                            next[1][x] -= h;
                        }
                        break;
                    case 3:
                        stack[top++] = (i, 0, 0, next[1][0]+1-h, unused);
                        for (var x=0; x<w; x++) {
                            next[1][x] -= h;
                        }
                        break;
                    }
                }

                i = -1;
                while (true) {
                    if (token.IsCancellationRequested) {
                        return (null, nodestats);
                    }

                    // find next placement location
                    // (cc, cx, cy, cw) = NextP1(n, next);
                    // (cc, cx, cy, cw) = NextP2(n, next);
                    // (cc, cx, cy, cw) = NextP3(n, next);
                    (cc, cx, cy, cw) = NextP4(n, next);
                    Debug.Assert(cc != 0 || cx == 0   || next[0][cx-1] > next[1][cx-1] || next[0][cx-1] > next[0][cx]);
                    Debug.Assert(cc != 1 || cx == n-1 || next[0][cx+1] > next[1][cx+1] || next[0][cx+1] > next[0][cx]);
                    Debug.Assert(cc != 2 || cx == n-1 || next[0][cx+1] > next[1][cx+1] || next[1][cx+1] < next[1][cx]);
                    Debug.Assert(cc != 3 || cx == 0   || next[0][cx-1] > next[1][cx-1] || next[1][cx-1] < next[1][cx]);
                    // Print(n, pieces, stack, top, taken, cc, cx, cy, cw, next);

                next:
                    // find next piece
                    do {
                        i++;
                        k = i >> 1;
                    } while (k < m && taken[k]);
                    if (k == m) {
                        goto backtrack;
                    }
                    (w, h) = pieces[i];
                    if (w == h && (i & 0x1) != 0) {
                        // prune symmetric solutions with transposed quadratic pieces
                        goto next; // continue w/o recomputing cx/cy
                    }
                    nodestats[top]++;
                    Debug.Assert(top > 0 || (i & 0x1) == 0/*, $"top={top} i={i}"*/);
                    // if (nodestats.Sum() == 200) {
                    //     return (null, nodestats);
                    // }
                    if (w > cw) {
                        goto next; // continue w/o recomputing cx/cy
                    }
                    // Console.WriteLine($"*** top = {top}, i = {i} (k = {k}), w = {w}, h = {h}, cx = {cx}, cy = {cy}");
                    switch (cc) {
                    case 0:
                        // check bounds
                        if (cx+w > n || cy+h > n) {
                            goto next; // continue w/o recomputing cx/cy
                        }
                        // check for piece intersections
                        for (var tx=cx; tx<cx+w; tx++) {
                            if (next[0][tx] > cy || next[1][tx] < cy+h-1) {
                                goto next; // continue w/o recomputing cx/cy
                            }
                        }
                        // update array of next placement locations
                        for (var tx=cx; tx<cx+w; tx++) {
                            next[0][tx] += h;
                        }
                        break;
                    case 1:
                        // check bounds
                        if (cx-w+1 < 0 || cy+h > n) {
                            goto next; // continue w/o recomputing cx/cy
                        }
                        // check for piece intersections
                        for (var tx=cx-w+1; tx<=cx; tx++) {
                            if (next[0][tx] > cy || next[1][tx] < cy+h-1) {
                                goto next; // continue w/o recomputing cx/cy
                            }
                        }
                        // update array of next placement locations
                        for (var tx=cx-w+1; tx<=cx; tx++) {
                            next[0][tx] += h;
                        }
                        break;
                    case 2:
                        // check bounds
                        if (cx-w+1 < 0 || cy-h+1 < 0) {
                            goto next; // continue w/o recomputing cx/cy
                        }
                        // check for piece intersections
                        for (var tx=cx-w+1; tx<=cx; tx++) {
                            if (next[0][tx] > cy-h+1 || next[1][tx] < cy) {
                                goto next; // continue w/o recomputing cx/cy
                            }
                        }
                        // update array of next placement locations
                        for (var tx=cx-w+1; tx<=cx; tx++) {
                            next[1][tx] -= h;
                        }
                        break;
                    case 3:
                        // check bounds
                        if (cx+w > n || cy-h+1 < 0) {
                            goto next; // continue w/o recomputing cx/cy
                        }
                        // check for piece intersections
                        for (var tx=cx; tx<cx+w; tx++) {
                            if (next[0][tx] > cy-h+1 || next[1][tx] < cy) {
                                goto next; // continue w/o recomputing cx/cy
                            }
                        }
                        // update array of next placement locations
                        for (var tx=cx; tx<cx+w; tx++) {
                            next[1][tx] -= h;
                        }
                        break;
                    default:
                        throw new InvalidOperationException($"Unsupported corner type: {cc}");
                    }
                    // place piece
                    stack[top++] = (i, cc, cx, cy, cw);
                    taken[k] = true;
                    // check if we've found a solution
                    if (top == m) {
                        var result = stack.Select(s => s.c switch {
                            0 => (s.x,                 s.y,                 pieces[s.i].w, pieces[s.i].h, s.i>>1),
                            1 => (s.x-pieces[s.i].w+1, s.y,                 pieces[s.i].w, pieces[s.i].h, s.i>>1),
                            2 => (s.x-pieces[s.i].w+1, s.y-pieces[s.i].h+1, pieces[s.i].w, pieces[s.i].h, s.i>>1),
                            3 => (s.x,                 s.y-pieces[s.i].h+1, pieces[s.i].w, pieces[s.i].h, s.i>>1),
                            _ => throw new InvalidOperationException($"Unsupported corner type: {s.c}")
                        }).ToArray();
                        return (result, nodestats);
                    }
                    // Console.WriteLine($"*** add top {top-1}: piece {stack[top-1].i} ({w} x {h}) at ({x},{y}), p = ({p.x},{p.y}), l = {lxy}");
                    // Print(n, pieces, stack, top, taken, cc, cx, cy, cw, next);
                    // find next placement location and continue search
                    i = -1;
                    continue;
                backtrack:
                    if (top == nc) {
                        break; // no solution found for current corner pieces, continue search with next corners
                    }
                    (i, cc, cx, cy, cw) = stack[--top];
                    k = i >> 1;
                    taken[k] = false;
                    (w, h) = pieces[i];
                    // Console.WriteLine($"*** del top {top}: piece {i} ({w} x {h}), p = ({p.x},{p.y}), l = {lxy}");
                    // update array of next placement locations
                    switch (cc) {
                    case 0:
                        for (var tx=cx; tx<cx+w; tx++) {
                            Debug.Assert(next[0][tx] == cy+h/*, $"next[0][tx] = {next[0][tx]}, y = {p.y}, h = {h}"*/);
                            next[0][tx] -= h;
                        }
                        break;
                    case 1:
                        for (var tx=cx-w+1; tx<=cx; tx++) {
                            Debug.Assert(next[0][tx] == cy+h/*, $"next[0][tx] = {next[0][tx]}, y = {p.y}, h = {h}"*/);
                            next[0][tx] -= h;
                        }
                        break;
                    case 2:
                        for (var tx=cx-w+1; tx<=cx; tx++) {
                            Debug.Assert(next[1][tx] == cy-h/*, $"next[0][tx] = {next[0][tx]}, y = {p.y}, h = {h}"*/);
                            next[1][tx] += h;
                        }
                        break;
                    case 3:
                        for (var tx=cx; tx<cx+w; tx++) {
                            Debug.Assert(next[1][tx] == cy-h/*, $"next[0][tx] = {next[0][tx]}, y = {p.y}, h = {h}"*/);
                            next[1][tx] += h;
                        }
                        break;
                    default:
                        throw new InvalidOperationException($"Unsupported corner type: {cc}");
                    }
                    goto next; // continue w/o recomputing cx/cy
                }                
            }
            return (null, nodestats);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Threading;

namespace Mondrian
{
    // Placement strategy 8
    //
    // 3 phase strategy
    //
    // (1) Try to place 4 corner pieces
    // (2) Place all edge pieces.
    // (3) Fill interior of square with a 'tightest corner' placement strategy.
    //
    // The 4 corner pieces in phase 1 are placed such that checking symmetric
    // solutions is prevented.
    public sealed unsafe partial class Placer8
    {
        private const int unused = -1;

        private CancellationToken token;

        private int nw; // rectangle width
        private int nh; // rectangle height

        private (int w,int h)[] candidates;
        private (int w,int h)[] pieces;

        private ulong taken;
        private (int,int,int)[] stack; // search stack (int is piece index, other tuple is phase dependent)
        
        private byte[] mem_horz_square;
        private byte[] mem_vert_square;
        private ulong* horz_square;
        private ulong* vert_square;

        private int ncorners;
        private int nedges;

        private long[] nodestats;

        private Placer8(int n, (int w,int h)[] candidates, CancellationToken token)
        {
            this.candidates = candidates;
            this.token = token;

            var m = candidates.Length;
            var m2 = 2*m;

            var ifw = candidates.TakeWhile(p => p.w != n).Count(); // index of full-width piece (or m, when no full-width piece)
            // rectangle dimensions
            this.nw = n;
            this.nh = n - (ifw < m ? candidates[ifw].h : 0);

            // oriented pieces
            var pieces = new (int w,int h)[m2];
            for (var i=0; i<m2; i++) {
                int w, h;
                if ((i & 0x1) == 0) {
                    (w, h) = candidates[i>>1];
                }
                else {
                    (h, w) = candidates[i>>1];
                }
                pieces[i] = (w, h);
            }
            this.pieces = pieces;

            // search state
            this.taken = 0UL;
            this.stack = new (int,int,int)[m];
            
            this.mem_horz_square = new byte[64*sizeof(ulong) + 31];
            this.mem_vert_square = new byte[64*sizeof(ulong) + 31];

            this.ncorners = m;
            this.nedges = m;

            // search statistics
            this.nodestats = new long[m];
        }

        private IEnumerable<(int x,int y,int w,int h,int i)> ReconstructSolution()
        {
            var t = 0;
            foreach (var c in ReconstructCorners()) {
                yield return c;
                t++;
            }
            foreach (var c in ReconstructEdges()) {
                yield return c;
                t++;
            }
            foreach (var c in ReconstructInterior()) {
                yield return c;
                t++;
            }
        }

        private void Print(int nelems, (int x,int y) p)
        {
            const string symbols = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Console.WriteLine($"*** stack: [{String.Join(", ", stack[0..nelems].Select(s=>symbols[s.Item1>>1]))}]");
            Console.WriteLine($"*** taken: [{String.Join(", ", Enumerable.Range(0, nelems).Select(tk => (taken & (1UL<<tk)) != 0 ? "1" : "0"))}]");
            Console.WriteLine($"*** pos: ({p.x},{p.y})");
            for (var y=0; y<nh; y++) {
                Console.Write("*** ");
                for (var x=0; x<nw; x++) {
                    var top = 0;
                    for (; top<nelems; top++) {
                        int ti, tx, ty, tw, th;
                        if (top < ncorners) {
                            int c;
                            (ti, c, _) = stack[top];
                            (tw, th) = pieces[ti];
                            switch (c) {
                            case -1: {
                                tx = 0;
                                ty = nw-th; // note: use 'nw' for full height
                                break;
                            }
                            case 0: {
                                tx = 0;
                                ty = 0;
                                break;
                            }
                            case 1: {
                                tx = nw-tw;
                                ty = 0;
                                break;
                            }
                            case 2: {
                                tx = nw-tw;
                                ty = nh-th;
                                break;
                            }
                            case 3: {
                                tx = 0;
                                ty = nh-th;
                                break;
                            }
                            default:
                                throw new InvalidOperationException($"Invalid corner value: c = {c}");
                            }
                        }
                        else if (top < nedges) {
                            // edge piece
                            int dir, pos;
                            (ti, dir, pos) = stack[top];
                            (tw, th) = pieces[ti];
                            switch (dir) {
                            case 0: {
                                tx = pos;
                                ty = 0;
                                break;
                            }
                            case 1: {
                                tx = nw-tw;
                                ty = pos;
                                break;
                            }
                            case 2: {
                                tx = pos-tw+1;
                                ty = nh-th;
                                break;
                            }
                            case 3: {
                                tx = 0;
                                ty = pos-th+1;
                                break;
                            }
                            default:
                                throw new InvalidOperationException($"Invalid edge value: dir = {dir}");
                            }
                        }
                        else {
                            // interior piece
                            (ti, tx, ty) = stack[top];
                            (tw, th) = pieces[ti];
                        }
                        if (tx <= x && x < tx+tw && ty <= y && y < ty+th) {
                            Console.Write(symbols[ti>>1]);
                            break;
                        }
                    }
                    if (top == nelems) {
                        if (x==p.x && y==p.y) {
                            Console.Write("*");
                        }
                        else {
                            Console.Write(".");
                        }
                    }
                }
                Console.Write("    ");
                var mask = 1UL;
                for (var x=0; x<nw; x++, mask <<= 1) {
                    if ((horz_square[y] & mask) != 0) {
                        Console.Write("*");
                    }
                    else {
                        Console.Write(".");
                    }
                }
                // ***DEBUG***
                // Transpose64((byte*)horz_square, (byte*)vert_square);
                // Console.Write("    ");
                // mask = 1UL;
                // for (var x=0; x<nw; x++, mask <<= 1) {
                //     if ((vert_square[y] & mask) != 0) {
                //         Console.Write("*");
                //     }
                //     else {
                //         Console.Write(".");
                //     }
                // }
                // ***ENDEBUG***
                Console.WriteLine();
            }
            // Environment.Exit(0);
        }

        public static ((int x,int y,int w,int h,int i)[]?, long[] nodestats) Find(int n, (int w,int h)[] candidates, CancellationToken token)
        {
            if (n >= 64) {
                throw new ArgumentException($"Unsupported problem size: expected n < 64, found n = {n}.", "n");
            }
            if (candidates.Length > 32) {
                throw new ArgumentException($"Unsupported number of candidates: expected m <= 32, found m = {candidates.Length}.", "candidates");
            }
            if (!Bmi1.X64.IsSupported) {
                throw new InvalidOperationException($"Unsupported instruction set architecture: missing feature 'bmi1'.");
            }

            var placer = new Placer8(n, candidates, token);

            fixed (byte* ptr_horz_square = placer.mem_horz_square)
            fixed (byte* ptr_vert_square = placer.mem_vert_square)
            {
                placer.horz_square = (ulong*)(((ulong)ptr_horz_square+31UL) & ~31UL); // 32 byte aligned ptr
                placer.vert_square = (ulong*)(((ulong)ptr_vert_square+31UL) & ~31UL); // 32 byte aligned ptr

                if (placer.FindCorners()) {
                    var result = placer.ReconstructSolution().ToArray();
                    return (result, placer.nodestats);
                }
                else {
                    return (null, placer.nodestats);
                }
            }

        }
    }
}
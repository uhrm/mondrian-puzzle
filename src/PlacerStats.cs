
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Mondrian
{
    public class PlacerStats
    {
        private long[] nodehist;

        public PlacerStats() : this(0) { }

        public PlacerStats(int n)
        {
            this.nodehist = new long[n];
        }

        public long NumNodes
        { 
            get { return this.nodehist.Sum(); }
        }

        public int MaxDepth
        {
            get { return this.nodehist.Length; }
        }

        public IReadOnlyList<long> NodeHist
        {
            get { return this.nodehist; }
        }

        public void Update(long[] stats)
        {
            lock (this) {
                var n = stats.Length;
                if (this.nodehist.Length < n) {
                    Array.Resize(ref this.nodehist, n);
                }
                for (var i=0; i<n; i++) {
                    this.nodehist[i] += stats[i];
                }
            }
        }
    }
}
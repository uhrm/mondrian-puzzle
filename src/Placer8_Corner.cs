using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Threading;

namespace Mondrian
{
    // Placement strategy 8
    //
    // Phase 1: Corner enumeration
    //
    // The 4 corner pieces are placed such that checking symmetric solutions
    // is prevented.
    public sealed partial class Placer8
    {
        // enumerate all 4 corner pieces in square domain
        private bool FindCornersSquare()
        {
            var pieces = this.pieces;
            (int i,int c,int)[] stack = this.stack;
            var nodestats = this.nodestats;
            // var token = this.token;

            var nw = this.nw;
            var nh = this.nh;
            Debug.Assert(nw == nh);

            var m = pieces.Length;

            var top = 0; // current search depth
            int c = 0; // search corner (0: top-left, 1: top-right, 2: bottom-right, 3: bottom-left)
            int i = -1, k, p = -1; // oriented piece index, piece index, pivot piece (on stack[0])
            while (true) {
                // find next piece
                i++;
                k = i >> 1;
                k = (int)Bmi1.X64.TrailingZeroCount(~(this.taken | ((1UL<<k)-1UL)));
                i = Math.Max(i, k<<1);
                // Console.WriteLine($"*** top = {top}, dir = {dir}, i = {i} (k = {k}), nfree = {nfree}");
                if (i == m) {
                    goto backtrack;
                }
                Debug.Assert(i < m && k < (m>>1));
                Debug.Assert(top == 0 || (stack[0].i >> 1) == p);
                var (w, h) = pieces[i];
                nodestats[top]++;
                if (w == h && (i&0x1) == 1) {
                    continue;
                }
                switch (c) {
                case 0:
                    if ((i & 0x1) != 0) {
                        continue; // eliminate symmetric solutions (transposition)
                    }
                    p = k;
                    stack[top++] = (i, c, unused);
                    c = w < nw ? 1 : 2;
                    break;
                case 1:
                    if (nw-pieces[stack[top-1].i].w < w) {
                        continue; // piece i does not fit in top right corner
                    }
                    stack[top++] = (i, c, unused);
                    c = h < nh ? 2 : 3;
                    break;
                case 2:
                    if (nh-pieces[stack[top-1].i].h < h) {
                        // Note: assuming diagonal pieces (0 and 2) never overlap
                        continue; // piece i does not fit in lower right corner
                    }
                    stack[top++] = (i, c, unused);
                    c = w < nw ? 3 : 4;
                    break;
                case 3:
                    if (nw-pieces[stack[top-1].i].w < w || nh-pieces[stack[0].i].h < h) {
                        // Note: assuming diagonal pieces (1 and 3) never overlap
                        continue; // piece i does not fit in lower left corner
                    }
                    stack[top++] = (i, c, unused);
                    c = 4;
                    break;
                default:
                    throw new InvalidOperationException();
                }
                taken |= 1UL << k;
                // Console.WriteLine($"*** corner add top = {top-1}, i = {i,2} (k = {k,2}), (w,h) = ({w,2},{h,2}), c = {stack[top-1].Item2.c}");
                if (c <= 3) {
                    // continue search at next corner
                    i = (p<<1) + 1;  // eliminate symmetric solutions (rotation)
                    continue;
                }
                // continue with phase 2 (edge placement)
                this.ncorners = top;
                if (FindEdges()) {
                    return true;
                }
                this.ncorners = stack.Length;
                // fall through to backtrack
            backtrack:
                if (top == 0) {
                    return false; // no solution found
                }
                (i, c, _) = stack[--top];
                k = i >> 1;
                taken &= ~(1UL << k);
                // Console.WriteLine($"*** corner del top = {top}, i = {i,2} (k = {k,2}), c = {c}");
            }
       }

        // place full-width piece at bottom edge and enumerate 4 corner pieces in rectangualr domain
        private bool FindCornersRect()
        {
            var pieces = this.pieces;
            (int i,int c,int)[] stack = this.stack;
            var nodestats = this.nodestats;
            // var token = this.token;

            var m = pieces.Length;

            int i, k, p = -1; // oriented piece index, piece index, pivot piece (on stack[0])

            // rectangle dimensions
            var nw = this.nw;
            var nh = this.nh;

            // place full-width piece at bottom
            i = pieces.TakeWhile(p => p.w != nw).Count();
            Debug.Assert(i < m);
            k = i >> 1;
            stack[0] = (i, -1, unused);
            this.taken |= 1UL << k;

            // enumerate 4 corner pieces
            var top = 1; // current search depth
            int c = 0; // search corner (0: top-left, 1: top-right, 2: bottom-right, 3: bottom-left)
            i = -1;
            while (true) {
                // find next piece
                i++;
                k = i >> 1;
                k = (int)Bmi1.X64.TrailingZeroCount(~(this.taken | ((1UL<<k)-1UL)));
                i = Math.Max(i, k<<1);
                // Console.WriteLine($"*** top = {top}, dir = {dir}, i = {i} (k = {k}), nfree = {nfree}");
                if (i == m) {
                    goto backtrack;
                }
                Debug.Assert(i < m && k < (m>>1));
                Debug.Assert(top == 1 || (stack[1].i >> 1) == p);
                var (w, h) = pieces[i];
                nodestats[top]++;
                switch (c) {
                case 0: {
                    Debug.Assert(nh >= h);
                    if (nh < h) {
                        continue; // piece i does not fit in top left corner
                    }
                    // if (nh == h) {
                    //     continue; // eliminate symmetric solutions
                    // }
                    p = k;
                    stack[top++] = (i, c, unused);
                    c = w < nw ? 1 : 2;
                    break;
                }
                case 1: {
                    Debug.Assert(nh >= h);
                    if (nw-pieces[stack[top-1].i].w < w || nh < h) {
                        continue; // piece i does not fit in top right corner
                    }
                    stack[top++] = (i, c, unused);
                    c = h < nh ? 2 : 3;
                    break;
                }
                case 2: {
                    if (nh-pieces[stack[top-1].i].h < h) {
                        // Note: assuming diagonal pieces (0 and 2) never overlap
                        continue; // piece i does not fit in lower right corner
                    }
                    stack[top++] = (i, c, unused);
                    c = w < nw ? 3 : 4;
                    break;
                }
                case 3: {
                    if (nw-pieces[stack[top-1].i].w < w || nh-pieces[stack[1].i].h < h) {
                        // Note: assuming diagonal pieces (1 and 3) never overlap
                        continue; // piece i does not fit in lower left corner
                    }
                    stack[top++] = (i, c, unused);
                    c = 4;
                    break;
                }
                default:
                    throw new InvalidOperationException($"Invalid corner value: c = {c}");
                }
                this.taken |= 1UL << k;
                // Console.WriteLine($"*** corner add top = {top}, i = {i,2} (k = {k,2}), (w,h) = ({w,2},{h,2}), c = {c}");
                if (c < 4) {
                // if (c < 3 || (c == 3 && pieces[stack[1].i].h < nh)) {
                    // continue search at next corner
                    i = (p<<1) + 1;  // eliminate symmetric solutions
                    continue;
                }
                this.ncorners = top;
                if (FindEdges()) {
                    return true;
                }
                this.ncorners = stack.Length;
                // fall through to backtrack
            backtrack:
                if (top == 1) {
                    return false; // no solution found
                }
                (i, c, _) = stack[--top];
                k = i >> 1;
                this.taken &= ~(1UL << k);
                // Console.WriteLine($"*** corner del top = {top}, i = {i,2} (k = {k,2}), c = {c}");
            }
       }
        
        private bool FindCorners()
        {
            if (this.nw != this.nh) {
                return FindCornersRect();
            }
            else {
                return FindCornersSquare();
            }
        }

        private IEnumerable<(int x,int y,int w,int h,int i)> ReconstructCorners()
        {
            var pieces = this.pieces;
            var stack = this.stack;

            var nw = this.nw;
            var nh = this.nh;

            var ncorners = this.ncorners;

            var m = stack.Length;

            var top = 0;
            while (top < ncorners) {
                var (i, c, _) = stack[top++];
                var (w, h) = pieces[i];
                switch (c) {
                case -1: {
                    yield return (0, nw-h, w, h, i>>1); // note: use 'nw' for full height
                    break;
                }
                case 0: {
                    yield return (0, 0, w, h, i>>1);
                    break;
                }
                case 1: {
                    yield return (nw-w, 0, w, h, i>>1);
                    break;
                }
                case 2: {
                    yield return (nw-w, nh-h, w, h, i>>1);
                    break;
                }
                case 3: {
                    yield return (0, nh-h, w, h, i>>1);
                    break;
                }
                default:
                    throw new InvalidOperationException($"Invalid corner value: c = {c}");
                }
            }
        }
    }
}
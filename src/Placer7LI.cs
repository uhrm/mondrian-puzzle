using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using System.Threading;

namespace Mondrian
{
    // Placement strategy 7
    //
    // Try to place 4 corner pieces, then fill rest of square with
    // top-most-then-left-most strategy.
    //
    // The 4 corner pieces are placed such that checking symmetric solutions
    // is prevented.
    public sealed class Placer7LI
    {
        // enumerate all 4 corner pieces in square domain
        private static IEnumerable<(int nc,(int i,int c)[])> FindCornersSquare(int n, (int w,int h)[] pieces, long[] nodestats)
        {
            var m2 = pieces.Length;
            var m = m2 >> 1;
            var taken = new bool[m];
            Array.Fill(taken, false);
            var stack = new (int i,int c)[] { (-1, -1), (-1, -1), (-1, -1), (-1, -1) };
            var top = 0; // current search depth
            int c = 0; // search corner (0: top-left, 1: top-right, 2: bottom-right, 3: bottom-left)
            int i = -1, k, p = -1; // oriented piece index, piece index, pivot piece (on stack[0])
            while (true) {
                // find next piece
                do {
                    i++;
                    k = i >> 1;
                } while (k < m && taken[k]);
                // Console.WriteLine($"*** top = {top}, dir = {dir}, i = {i} (k = {k}), nfree = {nfree}");
                if (k == m) {
                    goto backtrack;
                }
                Debug.Assert(k < m);
                Debug.Assert(top == 0 || (stack[0].i >> 1) == p);
                switch (c) {
                case 0: {
                    if ((i & 0x1) != 0) {
                        continue; // eliminate symmetric solutions (transposition)
                    }
                    else {
                        p = k;
                    }
                    break;
                }
                case 1: {
                    if (n-pieces[stack[top-1].i].w < pieces[i].w) {
                        continue; // piece i does not fit in top right corner
                    }
                    break;
                }
                case 2: {
                    if (n-pieces[stack[top-1].i].h < pieces[i].h) {
                        // Note: assuming diagonal pieces (0 and 2) never overlap
                        continue; // piece i does not fit in lower right corner
                    }
                    break;
                }
                case 3: {
                    var (w, h) = pieces[i];
                    if (n-pieces[stack[top-1].i].w < w || n-pieces[stack[0].i].h < h) {
                        // Note: assuming diagonal pieces (1 and 3) never overlap
                        continue; // piece i does not fit in lower left corner
                    }
                    break;
                }}
                nodestats[top]++;
                // Console.WriteLine($"*** corner add top = {top}, i = {i,2} (k = {k,2}), (w,h) = ({pieces[i].w,2},{pieces[i].h,2}), c = {c}");
                stack[top++] = (i, c);
                taken[k] = true;
                c = c switch {
                    0 => pieces[i].w < n ? 1 : 2,
                    1 => pieces[i].h < n ? 2 : 3,
                    2 => pieces[i].w < n ? 3 : 4,
                    3 => 4,
                    _ => throw new InvalidOperationException()
                };
                i = (p<<1) + 1;  // eliminate symmetric solutions
                if (c <= 3) {
                    continue;
                }
                yield return (top, stack);
                // fall through to backtrack
            backtrack:
                if (top == 0) {
                    break; // no solution found for piece p, continue search with next piece
                }
                (i, c) = stack[--top];
                k = i >> 1;
                taken[k] = false;
                // Console.WriteLine($"*** corner del top = {top}, i = {i,2} (k = {k,2}), c = {c}");
            }
       }

        // place full-width piece at bottom edge and enumerate 4 corner pieces in rectangualr domain
        private static IEnumerable<(int nc,(int i,int c)[])> FindCornersRect(int n, (int w,int h)[] pieces, long[] nodestats)
        {
            var m2 = pieces.Length;
            var m = m2 >> 1;
            var taken = new bool[m];
            Array.Fill(taken, false);
            var stack = new (int i,int c)[] { (-1, -1), (-1, -1), (-1, -1), (-1, -1), (-1, -1) };

            int i = -1, k, p = -1; // oriented piece index, piece index, pivot piece (on stack[0])

            // place full-width piece at bottom
            i = pieces.TakeWhile(p => p.w != n).Count();
            k = i >> 1;
            stack[0] = (i, -1);
            taken[k] = true;
            // rectangle dimensions
            var nw = n;
            var nh = n - pieces[i].h;

            // enumerate 4 corner pieces
            var top = 1; // current search depth
            int c = 0; // search corner (0: top-left, 1: top-right, 2: bottom-right, 3: bottom-left)
            i = -1;
            while (true) {
                // find next piece
                do {
                    i++;
                    k = i >> 1;
                } while (k < m && taken[k]);
                // Console.WriteLine($"*** top = {top}, dir = {dir}, i = {i} (k = {k}), nfree = {nfree}");
                if (k == m) {
                    goto backtrack;
                }
                Debug.Assert(k < m);
                Debug.Assert(top == 1 || (stack[1].i >> 1) == p);
                switch (c) {
                case 0: {
                    Debug.Assert(nh >= pieces[i].h);
                    if (nh < pieces[i].h) {
                        continue; // piece i does not fit in top left corner
                    }
                    // if (nh == pieces[i].h) {
                    //     continue; // eliminate symmetric solutions
                    // }
                    p = k;
                    break;
                }
                case 1: {
                    Debug.Assert(nh >= pieces[i].h);
                    if (nw-pieces[stack[top-1].i].w < pieces[i].w || nh < pieces[i].h) {
                        continue; // piece i does not fit in top right corner
                    }
                    break;
                }
                case 2: {
                    if (nh-pieces[stack[top-1].i].h < pieces[i].h) {
                        // Note: assuming diagonal pieces (0 and 2) never overlap
                        continue; // piece i does not fit in lower right corner
                    }
                    break;
                }
                case 3: {
                    var (w, h) = pieces[i];
                    if (nw-pieces[stack[top-1].i].w < w || nh-pieces[stack[1].i].h < h) {
                        // Note: assuming diagonal pieces (1 and 3) never overlap
                        continue; // piece i does not fit in lower left corner
                    }
                    break;
                }}
                nodestats[top]++;
                // Console.WriteLine($"*** corner add top = {top}, i = {i,2} (k = {k,2}), (w,h) = ({pieces[i].w,2},{pieces[i].h,2}), c = {c}");
                stack[top++] = (i, c);
                taken[k] = true;
                c = c switch {
                    0 => pieces[i].w < nw ? 1 : 2,
                    1 => pieces[i].h < nh ? 2 : 3,
                    2 => pieces[i].w < nw ? 3 : 4,
                    3 => 4,
                    _ => throw new InvalidOperationException($"Invalid corner value: c = {c}")
                };
                i = (p<<1) + 1;  // eliminate symmetric solutions
                if (c < 3 || (c == 3 && pieces[stack[1].i].h < nh)) {
                    continue;
                }
                yield return (top, stack);
                // fall through to backtrack
            backtrack:
                if (top == 1) {
                    break; // no solution found for piece p, continue search with next piece
                }
                (i, c) = stack[--top];
                k = i >> 1;
                taken[k] = false;
                // Console.WriteLine($"*** corner del top = {top}, i = {i,2} (k = {k,2}), c = {c}");
            }
       }
        
        private static IEnumerable<(int nc,(int i,int c)[])> FindCorners(int n, (int w,int h)[] pieces, long[] nodestats)
        {
            if (pieces.Any(p => p.w == n)) {
                return FindCornersRect(n, pieces, nodestats);
            }
            else {
                return FindCornersSquare(n, pieces, nodestats);
            }
        }

        public static unsafe ((int x,int y,int w,int h,int i)[]?, long[] nodestats) Find(int n, (int w,int h)[] candidates, CancellationToken token)
        {
            if (n > 32) {
                throw new ArgumentException($"Unsupported problem size: expected n <= 32, found n = {n}.", "n");
            }
            if (!Bmi1.X64.IsSupported) {
                throw new InvalidOperationException($"Unsupported instruction set architecture: missing feature 'bmi1'.");
            }
            if (!Avx2.X64.IsSupported) {
                throw new InvalidOperationException($"Unsupported instruction set architecture: missing feature 'avx2'.");
            }

            var m = candidates.Length;
            var m2 = 2*m;

            // search statistics
            var nodestats = new long[m];

            // oriented pieces
            var pieces = new (int w,int h)[m2];
            for (var l=0; l<m2; l++) {
                int w, h;
                if ((l & 0x1) == 0) {
                    (w, h) = candidates[l>>1];
                }
                else {
                    (h, w) = candidates[l>>1];
                }
                pieces[l] = (w, h);
            }

            // search state
            var nvec = (n+31) & ~31;
            Debug.Assert(31+nvec == 63);
            fixed (sbyte* NEXT0_PTR = &(new sbyte[31+nvec])[0])
            fixed (sbyte* NEXT1_PTR = &(new sbyte[31+nvec])[0])
            fixed (sbyte* SQUARE_MASK_PTR = &(new sbyte[31+n*n*nvec])[0]) {
                var square_mask = (sbyte*)(((long)SQUARE_MASK_PTR+31L) & ~31L);
                Debug.Assert(square_mask - SQUARE_MASK_PTR >= 0 && square_mask - SQUARE_MASK_PTR < 32);
                for (var x=0; x<n; x++) {
                    for (var w=1; w<=n-x; w++) {
                        for (var l=0; l<nvec; l++) {
                            square_mask[(x*n+w)*nvec+l] = l >= x && l < x+w ? (sbyte)-1 : (sbyte)0;
                        }
                    }
                }
                var next = new sbyte*[] {
                    (sbyte*)(((long)NEXT0_PTR+31L) & ~31L), // array of next y-placement position [top] (for all x positions)
                    (sbyte*)(((long)NEXT1_PTR+31L) & ~31L)  // array of next y-placement position [bottom] (for all x positions)
                };
                Debug.Assert(next[0] - NEXT0_PTR >= 0 && next[0] - NEXT0_PTR < 32);
                Debug.Assert(next[1] - NEXT1_PTR >= 0 && next[1] - NEXT1_PTR < 32);
                var n0s = new Span<sbyte>(next[0], nvec);
                var n1s = new Span<sbyte>(next[1], nvec);
                var taken = 0UL;
                var stack = new (int i,(int x,int y) p)[m];

                foreach (var (nc, corners) in FindCorners(n, pieces, nodestats)) {

                    // if (nc != 4 || corners[0].i != 0 || corners[1].i != 9 || corners[2].i != 4 || corners[3].i != 11) {
                    //     Console.WriteLine($"*** corner skipping: {String.Join(", ", corners.Take(nc).Select(s => $"{s.i} ({pieces[s.i].w} x {pieces[s.i].h})"))}");
                    //     continue;
                    // }
                    // Console.WriteLine($"*** corner checking: {String.Join(", ", corners.Take(nc).Select(s => $"{s.i} ({pieces[s.i].w} x {pieces[s.i].h})"))}");

                    n0s.Fill(0);
                    n1s.Fill((sbyte)(n-1));
                    taken = 0UL;
                    Array.Fill(stack, (-1, (-1, -1)));
                    var top = 0; // current search position
                    var p = (x: n, y: n); // current placement positions
                    int i = -1, k; // oriented piece index, piece index
                    int w, h; // piece width, height
                    for (var l=0; l<nc; l++) {
                        i = corners[l].i;
                        k = i >> 1;
                        (w, h) = pieces[i];
                        taken |= 1UL << k;
                        switch (corners[l].c) {
                        case -1:
                            // full-width piece at bottom edge
                            stack[top++] = (i, (0, n-h));
                            for (var x=0; x<w; x++) {
                                next[1][x] -= (sbyte)h;
                            }
                            break;
                        case 0:
                            stack[top++] = (i, (0, 0));
                            for (var x=0; x<w; x++) {
                                next[0][x] += (sbyte)h;
                            }
                            break;
                        case 1:
                            stack[top++] = (i, (n-w, 0));
                            for (var x=n-w; x<n; x++) {
                                next[0][x] += (sbyte)h;
                            }
                            break;
                        case 2:
                            stack[top++] = (i, (n-w, next[1][n-1]-h+1));
                            for (var x=n-w; x<n; x++) {
                                next[1][x] -= (sbyte)h;
                            }
                            break;
                        case 3:
                            stack[top++] = (i, (0, next[1][0]-h+1));
                            for (var x=0; x<w; x++) {
                                next[1][x] -= (sbyte)h;
                            }
                            break;
                        default:
                            throw new InvalidOperationException();
                        }
                    }
                    for (var x=0; x<n; x++) {
                        var n0x = next[0][x];
                        var n1x = next[1][x];
                        if (n0x <= n1x && n0x < p.y) {
                            p.x = x;
                            p.y = n0x;
                        }
                    }

                    i = -1;
                    while (true) {
                        if (token.IsCancellationRequested) {
                            return (null, nodestats);
                        }
                        // find next piece
                        i++;
                        k = i >> 1;
                        // Console.WriteLine($"*** next piece (1): i = {i,2} (k = {k,2}), taken = {taken:X16}, mask = {(1UL<<k)-1UL:X16} {~((1UL<<k)-1UL):X16}");
                        k = (int)Bmi1.X64.TrailingZeroCount(~(taken | ((1UL<<k)-1UL)));
                        i = Math.Max(i, k<<1);
                        // Console.WriteLine($"*** next piece (2): i = {i,2} (k = {k,2})");
                        if (k == m) {
                            goto backtrack;
                        }
                        (w, h) = pieces[i];
                        if (w == h && (i & 0x1) != 0) {
                            continue; // prune symmetric solutions with transposed quadratic pieces
                        }
                        nodestats[top]++;
                        Debug.Assert(top > 0 || (i & 0x1) == 0/*, $"top={top} i={i}"*/);
                        // if (nodestats.Sum() == 200) {
                        //     return (null, nodestats);
                        // }
                        // Console.WriteLine($"*** top = {top}, i = {i} (k = {k}), x = {x}, y = {y}, w = {w}, h = {h}");
                        // Try to place oriented piece (w,h) at position (x,y). If successful, find next placement position 'p'.
                        // check bounds
                        var (x, y) = p;
                        if (x+w > n || y+h > n) {
                            continue;
                        }
                        // check for piece intersections
                        // condition: (next[0][x..x+w] > y || next[1][x..x+w] < y+h-1)
                        // Console.WriteLine($"*** tryadd x = {x}, y = {y}, w = {w}, h = {h}");
                        var mv = Avx2.LoadAlignedVector256(square_mask + (x*n+w)*nvec);
                        // Console.WriteLine($"*** tryadd wv = {wv}");
                        var y0vm = Avx2.And(Avx2.BroadcastScalarToVector256(Avx2.ConvertScalarToVector128Int32(y).AsSByte()), mv);
                        // Console.WriteLine($"*** tryadd y0v = {y0v}");
                        var n0v = Avx2.LoadAlignedVector256(next[0]);
                        var n0vm = Avx2.And(n0v, mv);
                        // Console.WriteLine($"*** tryadd n0v = {n0v}");
                        var y1vm = Avx2.And(Avx2.BroadcastScalarToVector256(Avx2.ConvertScalarToVector128Int32(y+h-1).AsSByte()), mv);
                        // Console.WriteLine($"*** tryadd y1v = {y1v}");
                        var n1v = Avx2.LoadAlignedVector256(next[1]);
                        var n1vm = Avx2.And(n1v, mv);
                        // Console.WriteLine($"*** tryadd n1v = {n1v}");
                        var mask = Avx2.MoveMask(
                            Avx2.Or(
                                Avx2.CompareGreaterThan(n0vm, y0vm),
                                Avx2.CompareGreaterThan(y1vm, n1vm)
                            )
                        );
                        // Console.WriteLine($"*** tryadd mask = {mask:X8}");
                        if (mask != 0) {
                            continue;
                        }
                        // update array of next placement locations
                        // operation: next[0][x..x+w] += h
                        var hvm = Avx2.And(Avx2.BroadcastScalarToVector256(Avx2.ConvertScalarToVector128Int32(h).AsSByte()), mv);
                        n0v = Avx2.Add(n0v, hvm);
                        Avx2.StoreAligned(next[0], n0v);
                        // find next placement location
                        // Console.WriteLine($"*** tryadd n0v = {n0v}");
                        // compute mask of relevant elements
                        // condition: 0 <= x < w && next[0][x] <= next[1][x]   for x = 0,..,n-1
                        var minvm = Avx2.AndNot(Avx2.CompareGreaterThan(n0v, n1v), Avx2.LoadAlignedVector256(square_mask + n*nvec));
                        // Console.WriteLine($"*** tryadd minvm = {minvm}");
                        // set non-mask elements to n
                        n0v =  Avx2.Or(Avx2.And(minvm, n0v), Avx2.AndNot(minvm, Avx2.BroadcastScalarToVector256(Avx2.ConvertScalarToVector128Int32(n).AsSByte())));
                        // convert to 16bit numbers (for horizontal min)
                        var n0v16lo = Avx2.ConvertToVector256Int16(Avx2.ExtractVector128(n0v, 0));
                        var n0v16hi = Avx2.ConvertToVector256Int16(Avx2.ExtractVector128(n0v, 1));
                        // Console.WriteLine($"*** tryadd n0v = {n0v}");
                        // Console.WriteLine($"*** tryadd n0v16lo = {n0v16lo}");
                        // Console.WriteLine($"*** tryadd n0v16hi = {n0v16hi}");
                        // find minimum element of n0v
                        var minv = Avx2.MinHorizontal(Avx2.Min(
                                Avx2.Min(
                                    Avx2.ExtractVector128(n0v16lo, 0),
                                    Avx2.ExtractVector128(n0v16hi, 0)
                                ),
                                Avx2.Min(
                                    Avx2.ExtractVector128(n0v16lo, 1),
                                    Avx2.ExtractVector128(n0v16hi, 1)
                                )
                            ).AsUInt16()).AsByte();
                        // store minimum element in p.y
                        p.y = Avx2.Extract(minv, 0);
                        // store index of minimum element in p.x
                        p.x = (int)Bmi1.TrailingZeroCount(
                            (uint)Avx2.MoveMask(
                                Avx2.CompareEqual(
                                    n0v,
                                    Avx2.BroadcastScalarToVector256(minv).AsSByte()
                                )));
                        // place piece
                        stack[top++] = (i, (x, y));
                        taken |= 1UL << k;
                        // Console.WriteLine($"*** add top {top-1}: piece {stack[top-1].i} ({w} x {h}) at ({x},{y}), p = ({p.x},{p.y})");
                        // Print(n, pieces, stack, top, taken, p, next);
                        // check if we've found a solution
                        if (top == m) {
                            var result = stack.Select(s => (s.p.x, s.p.y, pieces[s.i].w, pieces[s.i].h, s.i>>1)).ToArray();
                            return (result, nodestats);
                        }
                        // continue search at next placement location
                        i = -1;
                        continue;
                    backtrack:
                        if (top == nc) {
                            break; // no solution found for current corner pieces, continue search with next corners
                        }
                        (i, p) = stack[--top];
                        k = i >> 1;
                        taken &= ~(1UL << k);
                        (w, h) = pieces[i];
                        // update array of next placement locations
                        // operation: next[0][x..x+w] -= h
                        Avx2.StoreAligned(next[0],
                            Avx2.Subtract(
                                Avx2.LoadAlignedVector256(next[0]), 
                                Avx2.And(
                                    Avx2.BroadcastScalarToVector256(Avx2.ConvertScalarToVector128Int32(h).AsSByte()),
                                    Avx2.LoadAlignedVector256(square_mask + (p.x*n+w)*nvec))));
                        // Console.WriteLine($"*** del top {top}: piece {i} ({w} x {h}) at ({p.x},{p.y})");
                    }                
                }
            }
            return (null, nodestats);
        }

        private static unsafe void Print(int n, (int w,int h)[] pieces, (int i,(int x,int y) p)[] stack, int top, ulong taken, (int x,int y) p, sbyte*[] next)
        {
            const string symbols = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Console.WriteLine($"*** stack: [{String.Join(", ", stack[0..top].Select(s=>symbols[s.i>>1]))}]");
            Console.WriteLine($"*** taken: [{String.Join(", ", Enumerable.Range(0, pieces.Length>>1).Select(tk => (taken & (1UL<<tk)) != 0 ? "1" : "0"))}]");
            Console.WriteLine($"*** p = ({p.x},{p.y})");
            for (var y=0; y<n; y++) {
                Console.Write("*** ");
                for (var x=0; x<n; x++) {
                    var t = 0;
                    for (; t<top; t++) {
                        int ti, tx, ty, tw, th;
                        (ti, (tx, ty)) = stack[t];
                        (tw, th) = pieces[ti];
                        if (tx <= x && x < tx+tw && ty <= y && y < ty+th) {
                            Console.Write(symbols[ti>>1]);
                            break;
                        }
                    }
                    if (t == top) {
                        if (x==p.x && y==p.y) {
                            Console.Write("*");
                        }
                        else if (next[0][x] == y || next[1][x] == y) {
                            Console.Write("~");
                        }
                        else {
                            Console.Write(".");
                        }
                    }
                }
                Console.WriteLine();
            }
        }
   }
}
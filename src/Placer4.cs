using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using System.Threading;

namespace Mondrian
{
    public sealed class Placer4
    {
        // Try to place oriented piece (w,h) at position (x,y). If
        // successful, the functions returns true and updates the
        // position variables 'x', 'y' with the location where to
        // place the next piece. Otherwise, the function returns
        // false and does not modify the position variables 'x', 'y'.
        private static bool TryAdd(int n, int pos, int x, int y, int w, int h, int[][] next, (int x,int y)[] p)
        {
            switch (pos) {
            case 0:
                // pos=0: (x,y) denotes top-left corner
                // check bounds
                if (x+w > n || y+h > n) {
                    return false;
                }
                // check for piece intersections
                for (var tx=x; tx<x+w; tx++) {
                    if (next[0][tx] > y || next[1][tx]+1 < y+h) {
                        return false;
                    }
                }
                // update array of next placement locations
                for (var tx=x; tx<x+w; tx++) {
                    next[0][tx] += h;
                    // if (next[0][tx] > next[1][tx]) {
                    //     next[0][tx] = n;
                    //     next[1][tx] = -1;
                    // }
                }
                break;
            case 1:
                // pos=1: (x,y) denotes top-right corner
                // check bounds
                if (x-w < -1 || y+h > n) {
                    return false;
                }
                // check for piece intersections
                for (var tx=x; tx>x-w; tx--) {
                    if (next[0][tx] > y || next[1][tx]+1 < y+h) {
                        return false;
                    }
                }
                // update array of next placement locations
                for (var tx=x; tx>x-w; tx--) {
                    next[0][tx] += h;
                    // if (next[0][tx] > next[1][tx]) {
                    //     next[0][tx] = n;
                    //     next[1][tx] = -1;
                    // }
                }
                break;
            case 2:
                // pos=2: (x,y) denotes bottom-left corner
                // check bounds
                if (x+w > n || y-h < -1) {
                    return false;
                }
                // check for piece intersections
                for (var tx=x; tx<x+w; tx++) {
                    if (next[0][tx]-1 > y-h || next[1][tx] < y) {
                        return false;
                    }
                }
                // update array of next placement locations
                for (var tx=x; tx<x+w; tx++) {
                    next[1][tx] -= h;
                    // if (next[0][tx] > next[1][tx]) {
                    //     next[0][tx] = n;
                    //     next[1][tx] = -1;
                    // }
                }
                break;
            case 3:
                // pos=3: (x,y) denotes bottom-right corner
                // check bounds
                if (x-w < -1 || y-h < -1) {
                    return false;
                }
                // check for piece intersections
                for (var tx=x; tx>x-w; tx--) {
                    if (next[0][tx]-1 > y-h || next[1][tx] < y) {
                        return false;
                    }
                }
                // update array of next placement locations
                for (var tx=x; tx>x-w; tx--) {
                    next[1][tx] -= h;
                    // if (next[0][tx] > next[1][tx]) {
                    //     next[0][tx] = n;
                    //     next[1][tx] = -1;
                    // }
                }
                break;
            }
            // find next placement locations
            p[0].x = -1;
            p[0].y = Int32.MaxValue;
            p[2].x = -1;
            p[2].y = Int32.MinValue;
            for (var k=0; k<n; k++) {
                var n0k = next[0][k];
                var n1k = next[1][k];
                if (n0k <= n1k && n0k < p[0].y) {
                    p[0].x = k;
                    p[0].y = n0k;
                }
                if (n1k >= n0k && n1k > p[2].y) {
                    p[2].x = k;
                    p[2].y = n1k;
                }
            }
            p[1].x = -1;
            p[1].y = Int32.MaxValue;
            p[3].x = -1;
            p[3].y = Int32.MinValue;
            for (var k=n-1; k>=0; k--) {
                var n0k = next[0][k];
                var n1k = next[1][k];
                if (n0k <= n1k && n0k < p[1].y) {
                    p[1].x = k;
                    p[1].y = n0k;
                }
                if (n1k >= n0k && n1k > p[3].y) {
                    p[3].x = k;
                    p[3].y = n1k;
                }
            }
            return true;
        }

        private static void Remove(int n, int pos, int x, int y, int w, int h, int[][] next)
        {
            // update array of next placement locations
            switch (pos) {
            case 0:
                // pos=0: (x,y) denotes top-left corner
                for (var tx=x; tx<x+w; tx++) {
                    Debug.Assert(next[0][tx] == y+h);
                    next[0][tx] -= h;
                }
                break;
            case 1:
                // pos=1: (x,y) denotes top-right corner
                for (var tx=x; tx>x-w; tx--) {
                    Debug.Assert(next[0][tx] == y+h);
                    next[0][tx] -= h;
                }
                break;
            case 2:
                // pos=2: (x,y) denotes bottom-left corner
                for (var tx=x; tx<x+w; tx++) {
                    Debug.Assert(next[1][tx] == y-h);
                    next[1][tx] += h;
                }
                break;
            case 3:
                // pos=3: (x,y) denotes bottom-right corner
                for (var tx=x; tx>x-w; tx--) {
                    Debug.Assert(next[1][tx] == y-h);
                    next[1][tx] += h;
                }
                break;
            }
        }

        public static ((int x,int y,int w,int h,int i)[]?, long[] nodestats) Find(int n, (int w,int h)[] candidates, CancellationToken token)
        {
            var m = candidates.Length;
            var m2 = 2*m;

            // search statistics
            var nodestats = new long[m];

            // oriented pieces
            var pieces = new (int w,int h)[m2];
            for (var i=0; i<m2; i++) {
                var k = i >> 1;
                int w, h;
                if ((i & 0x1) == 0) {
                    (w, h) = candidates[k];
                }
                else {
                    (h, w) = candidates[k];
                }
                pieces[i] = (w, h);
            }

            var p = new (int x,int y)[4]; // current placement positions
            var next = new int[][] {
                Enumerable.Repeat(  0, n).ToArray(), // array of next y-placement position [top] (for all x positions)
                Enumerable.Repeat(n-1, n).ToArray()  // array of next y-placement position [bottom] (for all x positions)
            };
            var depth32 = new int[m2];
            var depth64 = MemoryMarshal.Cast<int,long>(depth32);
            Array.Fill(depth32, -1);
            var stack = new ((int x,int y) p0,(int x,int y) p1,(int x,int y) p2,(int x,int y) p3,int i)[m];
            Array.Fill(stack, ((-1, -1), (-1, -1), (-1, -1), (-1, -1), -1));
            var top = 0; // current depth
            while (top >= 0 && top < m && !token.IsCancellationRequested) {
                var (_, _, _, _, i) = stack[top];
                // find next piece to try
                while (true) {
                    i++;
                    var k = i >> 1; // piece index
                    while (k < m && depth64[k] != -1L) {
                        k++;
                        i = k << 1;
                    }
                    if (i >= m2) {
                        break;
                    }
                    if (top == 0 && (i & 0x1) != 0) {
                        // Console.WriteLine($"*** prune transpose (square 0): top={top} pos={top&0x3} i={i}");
                        continue; // prune symmetric solutions mirrored at diagonal
                    }
                    var (w, h) = pieces[i];
                    if (w == h &&  (i & 0x1) != 0) {
                        // Console.WriteLine($"*** prune square piece: top={top} pos={top&0x3} i={i}");
                        continue; // prune symmetric solutions with transposed quadratic pieces
                    }
                    if (top > 0) {
                        var i0 = stack[0].i;
                        if (/*top >= 1 &&*/ top <= 3 && i0 > i) {
                            // Console.WriteLine($"*** prune by index: top={top} pos={top&0x3} i={i}, i0={i0}");
                            continue; // prune symmetric solutions with smallest index piece not in top-left corner
                        }
                        var (w0, h0) = pieces[i0];
                        if (top == 2 && w0 == h0 && stack[1].i > i) {
                            // Console.WriteLine($"*** prune transpose (square 2): top={top} pos={top&0x3} i={i}, i1={stack[1].i}");
                            continue; // prune symmetric solutions mirrored at diagonal
                        }
                    }
                    break;
                }
                if (i < m2) {
                    nodestats[top]++;
                    Debug.Assert(top > 0 || (i & 0x1) == 0, $"top={top} i={i}");
                    // if (nodestats.Sum() == 200) {
                    //     return (null, nodestats);
                    // }
                    var pos = top & 0x3;
                    var (x, y) = p[pos];
                    stack[top] = (p[0], p[1], p[2], p[3], i);
                    var (w, h) = pieces[i];
                    // Console.WriteLine($"*** top = {top}, pos = {pos}, i = {i}, x = {x}, y = {y}, w = {w}, h = {h}");
                    if (TryAdd(n, pos, x, y, w, h, next, p)) {
                        // Console.WriteLine($"*** add top {top}: piece {i} ({w} x {h}) at ({x},{y})");
                        // place piece
                        depth32[i] = top;
                        top++;
                        // Print(n, pieces, stack, top, p, next);
                        if (top == m) {
                            var result = stack.Select((s, d) => (d & 0x3) switch {
                                0 => (s.p0.x,                 s.p0.y,                 pieces[s.i].w, pieces[s.i].h, s.i>>1),
                                1 => (s.p1.x-pieces[s.i].w+1, s.p1.y,                 pieces[s.i].w, pieces[s.i].h, s.i>>1),
                                2 => (s.p2.x,                 s.p2.y-pieces[s.i].h+1, pieces[s.i].w, pieces[s.i].h, s.i>>1),
                                3 => (s.p3.x-pieces[s.i].w+1, s.p3.y-pieces[s.i].h+1, pieces[s.i].w, pieces[s.i].h, s.i>>1),
                                _ => throw new InvalidOperationException()
                            }).ToArray();
                            return (result, nodestats);
                        }
                        // if (y == 0 && x+w == n) {
                        //     // prune symmetric solutions mirrored at y-axis
                        //     if ((prune[i] & 0x2) == 0) {
                        //         Console.WriteLine($"*** prune piece {i} [y] (stack: [{String.Join(", ", stack[0..top].Select(s=>s.i))}])");
                        //         Print(n, pieces, stack, top, (x, y));
                        //     }
                        //     prune[i] |= 0x2;
                        //     // prune symmetric solutions rotated by 90°
                        //     if ((prune[i^1] & 0x2) == 0) {
                        //         Console.WriteLine($"*** prune piece {i^1} [+] (stack: [{String.Join(", ", stack[0..top].Select(s=>s.i))}])");
                        //         Print(n, pieces, stack, top, (x, y));
                        //     }
                        //     prune[i^1] |= 0x2;
                        // }
                        // else if (x == 0 && y+h == n) {
                        //     // prune symmetric solutions mirrored at x-axis
                        //     if ((prune[i] & 0x2) == 0) {
                        //         Console.WriteLine($"*** prune piece {i} [x] (stack: [{String.Join(", ", stack[0..top].Select(s=>s.i))}])");
                        //         Print(n, pieces, stack, top, (x, y));
                        //     }
                        //     prune[i] |= 0x2;
                        //     // prune symmetric solutions rotated by -90°
                        //     if ((prune[i^1] & 0x2) == 0) {
                        //         Console.WriteLine($"*** prune piece {i^1} [-] (stack: [{String.Join(", ", stack[0..top].Select(s=>s.i))}])");
                        //         Print(n, pieces, stack, top, (x, y));
                        //     }
                        //     prune[i^1] |= 0x2;
                        // }
                    }
                }
                else {
                    // backtrack
                    stack[top] = ((-1, -1), (-1, -1), (-1, -1), (-1, -1), -1);
                    top--;
                    if (top >= 0) {
                        (p[0], p[1], p[2], p[3], i) = stack[top];
                        depth32[i] = -1;
                        var (w, h) = pieces[i];
                        // Console.WriteLine($"*** del top {top}: piece {i} ({w} x {h}) at ({x},{y})");
                        var pos = top & 0x3;
                        var (x, y) = p[pos];
                        Remove(n, pos, x, y, w, h, next);
                    }
                }
            }
            return (null, nodestats);
        }

        private static void Print(int n, (int w,int h)[] pieces, ((int x,int y) p0,(int x,int y) p1,(int x,int y) p2,(int x,int y) p3,int i)[] stack, int top, (int x,int y)[] p, int[][] next)
        {
            const string symbols = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Console.WriteLine($"*** stack: [{String.Join(", ", stack[0..top].Select(s=>symbols[s.i>>1]))}]");
            Console.WriteLine($"*** p = ({p[0].x},{p[0].y}), ({p[1].x},{p[1].y}), ({p[2].x},{p[2].y}), ({p[3].x},{p[3].y})");
            for (var y=0; y<n; y++) {
                Console.Write("*** ");
                for (var x=0; x<n; x++) {
                    var t = 0;
                    for (; t<top; t++) {
                        var pos = t & 0x3;
                        int tx = 0, ty = 0, ti = 0;
                        int tw = 0, th = 0;
                        switch (pos) {
                        case 0:
                            ((tx, ty), _, _, _, ti) = stack[t];
                            (tw, th) = pieces[ti];
                            // Console.WriteLine($"\n+++ tx={tx} ty={ty} tw={tw} th={th} ti={ti}");
                            break;
                        case 1:
                            (_, (tx, ty), _, _, ti) = stack[t];
                            (tw, th) = pieces[ti];
                            tx -= tw-1;
                            break;
                        case 2:
                            (_, _, (tx, ty), _, ti) = stack[t];
                            (tw, th) = pieces[ti];
                            ty -= th-1;
                            break;
                        case 3:
                            (_, _, _, (tx, ty), ti) = stack[t];
                            (tw, th) = pieces[ti];
                            tx -= tw-1;
                            ty -= th-1;
                            break;
                        }
                        if (tx <= x && x < tx+tw && ty <= y && y < ty+th) {
                            Console.Write(symbols[ti>>1]);
                            break;
                        }
                    }
                    if (t == top) {
                        if (p.Any(p => x==p.x && y==p.y)) {
                            Console.Write("*");
                        }
                        else if (next[0][x] == y || next[1][x] == y) {
                            Console.Write("~");
                        }
                        else {
                            Console.Write(".");
                        }
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
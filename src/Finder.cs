using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Mondrian
{
    public sealed class Finder 
    {
        public static Dictionary<int,(int w,int h)[]> GeneratePieces(int n)
        {
            return Enumerable.Range(1, n).SelectMany(w => Enumerable.Range(1, w).Select(h => (w,h)))
                    .GroupBy(t => t.w*t.h)
                    .ToDictionary(g => g.Key, g => g.ToArray());
        }

        private static IEnumerable<(int w,int h)[]> FindCandidates(int n, (int w,int h)[] pieces)
        {
            var nn = n*n;
            var m = pieces.Length;
            var W = pieces.Select(p => p.w*p.h).ToArray();
            if (W.Sum() < n*n) {
                yield break;
            }
            // ***DEBUG***
            // for (var i=1; i<m; i++) {
            //     Debug.Assert(W[i] >= W[i-1]);
            // }
            // ***ENDEBUG***
            // build subset sum table
            var F = new int[nn+1,m];
            for (var i=0; i<=nn; i++) {
                F[i,0] = i < W[0] ? 0 : W[0];
            }
            for (var j=1; j<m; j++) {
                for (var i=0; i<=nn; i++) {
                    var wj = W[j];
                    F[i,j] = i < wj ? F[i,j-1] : Math.Max(F[i,j-1], F[i-wj,j-1]+wj);
                }
            }
            // enumerate solutions
            var stack = new int[m];
            var top = 0;
            var c = nn;
            stack[top] = -1;
            while (true) {
                var mj = top == 0 ? m : stack[top-1];
                var j = stack[top];
                var wj = -1;
                // find next feasible piece
                do {
                    j++;
                    if (j == mj) {
                        break;
                    }
                    wj = W[j];
                    if (c >= wj && F[c,j] == c && (j == 0 || F[c,j] == F[c-wj,j-1]+wj)) {
                        break;
                    }
                } while (true);
                // take piece
                if (j < mj) {
                    stack[top++] = j;
                    // Console.WriteLine($"*** stack: [{String.Join(", ", stack[0..top])}] (top = {top}, mj = {mj}, c = {c})");
                    c -= wj;
                    Debug.Assert(c >= 0);
                    if (c > 0) {
                        Debug.Assert(top >= 0);
                        stack[top] = -1;
                        continue;
                    }
                    if (c == 0) {
                        // Console.WriteLine($"*** finder stack: [{String.Join(", ", stack[0..top])}] (top = {top})");
                        var candidate = Enumerable.Range(0, top).Select(j => pieces[stack[top-j-1]]).ToArray();
                        var len = candidate.Length;
                        if (candidate[0].w*candidate[0].h == W[0] && candidate[len-1].w*candidate[len-1].h == W[m-1]) {
                            // sort least square to most square
                            // Array.Sort(candidate, new Comparison<(int w,int h)>((ca, cb) => cb.w.CompareTo(ca.w)));

                            // sort most square to least square
                            // Array.Sort(candidate, new Comparison<(int w,int h)>((ca, cb) => ca.w.CompareTo(cb.w)));

                            // sort most square (excl. squares) to least square, then squares smallest to largest
                            Array.Sort(candidate, new Comparison<(int w,int h)>((ca, cb) => !(ca.w==ca.h ^ cb.w==cb.h) ? ca.w.CompareTo(cb.w) : ca.w==ca.h ? 1 : -1));

                            // sort largest to smallest (excl. squares), then squares largest to smallest
                            // Array.Sort(candidate, new Comparison<(int w,int h)>((ca, cb) => !(ca.w==ca.h ^ cb.w==cb.h) ? (cb.w*cb.h).CompareTo(ca.w*ca.h) : ca.w==ca.h ? 1 : -1));

                            // sort smallest to largest (excl. squares), then squares smallest to largest
                            // Array.Sort(candidate, new Comparison<(int w,int h)>((ca, cb) => !(ca.w==ca.h ^ cb.w==cb.h) ? (ca.w*ca.h).CompareTo(cb.w*cb.h) : ca.w==ca.h ? 1 : -1));

                            // 4 most square (excl. squares), then least square to most square, then squares largest to smallest
                            // Array.Sort(candidate, new Comparison<(int w,int h)>((ca, cb) => !(ca.w==ca.h ^ cb.w==cb.h) ? ca.w.CompareTo(cb.w) : ca.w==ca.h ? 1 : -1));
                            // var ilo = 4;
                            // var iup = len-1;
                            // while (candidate[iup].w == candidate[iup].h) {
                            //     iup--;
                            // }
                            // while (ilo < iup) {
                            //     (candidate[ilo], candidate[iup]) = (candidate[iup], candidate[ilo]);
                            //     ilo++;
                            //     iup--;
                            // }
                            yield return candidate;
                        }
                    }
                }
                // backtrack and continue search
                top--;
                if (top < 0) {
                    break;
                }
                c += W[stack[top]];
                // Console.WriteLine($"*** stack: [{String.Join(", ", stack[0..top])}] (top = {top})");
            }
        }

        // enumerate candidate pieces (smallest first)
        public static IEnumerable<(int w,int h)[]> FindCandidatesSF(int n, int v)
        {
            var pieces = GeneratePieces(n);
            var nn = n*n;
            for (var lb=0; lb<=nn-v; lb++) {
                if (!pieces.ContainsKey(lb) || !pieces.ContainsKey(lb+v)) {
                    continue;
                }
                var pcs = Enumerable.Range(lb, v+1).SelectMany(i => pieces.GetValueOrDefault(i) ?? Array.Empty<(int w,int h)>()).ToArray();
                foreach (var candidate in FindCandidates(n, pcs)) {
                    yield return candidate;
                }
            }
        }

        // enumerate candidate pieces (largest first)
        public static IEnumerable<(int w,int h)[]> FindCandidatesLF(int n, int v)
        {
            var pieces = GeneratePieces(n);
            var nn = n*n;
            for (var ub=nn-1; ub>v; ub--) {
                if (!pieces.ContainsKey(ub) || !pieces.ContainsKey(ub-v)) {
                    continue;
                }
                var pcs = Enumerable.Range(0, v+1).Select(i => ub-v+i).SelectMany(i => pieces.GetValueOrDefault(i) ?? Array.Empty<(int w,int h)>()).ToArray();
                // Console.WriteLine($"*** new pieces: {ub-v}-{ub}");
                foreach (var candidate in FindCandidates(n, pcs)) {
                    // var m = candidate.Length;
                    // if (candidate[0].w*candidate[0].h != ub-v || candidate[m-1].w*candidate[m-1].h != ub) {
                    //     continue;
                    // }
                    yield return candidate;
                }
            }
        }
    }
}
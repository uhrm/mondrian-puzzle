
using System;
using System.Linq;

namespace Mondrian
{
    public class Filter
    {
        public static bool Check(int n, (int w,int h)[] pieces, out int conflict)
        {
            var m = pieces.Length;
            var t = new bool[m];
            var stack = new (int i,int d)[m];
            var nrem = new int[2];
            for (var c=0; c<m; c++) {
                Array.Fill(t, false);
                t[c] = true;
                Array.Fill(stack, (-1, -1));
                nrem[0] = n - pieces[c].w;
                nrem[1] = n - pieces[c].h;
                var top = 0;
                var dir = nrem[0] > 0 ? 0 : 1; // 0: horizontal, 1: vertical
                int i = -1, k; // oriented piece index, piece index
                do {
                    do {
                        i++;
                        k = i >> 1;
                    } while (k < m && t[k]);
                    // Console.WriteLine($"*** top = {top}, dir = {dir}, i = {i} (k = {k}), nrem = ({nrem[0]}, {nrem[1]})");
                    if (k < m) {
                        var hs = (i & 0x1) == 0 ? pieces[k].w : pieces[k].h;
                        if (nrem[dir] >= hs) {
                            stack[top++] = (i, dir);
                            t[k] = true;
                            i = -1;
                            nrem[dir] -= hs;
                            if (nrem[dir] == 0) {
                                dir++;
                                if (dir > 1) {
                                    break; // solution found for piece c
                                }
                            }
                        }
                    }
                    else {
                        if (top == 0) {
                            // Console.WriteLine($"*** Filter: no solution found for piece {c} ({pieces[c].w} x {pieces[c].h}).");
                            conflict = c;
                            return false;
                        }
                        (i, dir) = stack[--top];
                        k = i >> 1;
                        t[k] = false;
                        nrem[dir] += (i & 0x1) == 0 ? pieces[k].w : pieces[k].h;
                    }
                } while (top >= 0);
            }
            conflict = -1;
            return true;
        }
    }
}